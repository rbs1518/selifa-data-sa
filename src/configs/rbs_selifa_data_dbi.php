<?php
return array(
    'DefaultDatabase' => 'Default',
    'DisableError' => false,
    'Connections' => array(
        'Default' => array(
            'type' => 'mysql',
			'host' => 'localhost',
			'name' => 'test',
			'user' => 'root',
			'pass' => ''
        )
    )
);
?>
