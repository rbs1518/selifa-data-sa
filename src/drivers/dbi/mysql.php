<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

use \RBS\Selifa\Data\BaseDatabaseDriver;
use \RBS\Selifa\Data\BaseDialect;

/**
 * Class MySQLDialect
 *
 * @package RBS\Selifa\Data
 */
class MySQLDialect extends BaseDialect
{
    /**
     *
     */
    function __construct()
    {
        $this->S = '`';
        $this->E = '`';
    }

    /**
     * @param string $sql
     * @param int $offset
     * @param int $count
     * @return string
     */
    public function LimitQuery($sql, $offset, $count)
    {
        if ($offset > 0)
            return ($sql.' limit '.$offset.', '.$count);
        else
            return ($sql.' limit '.$count);
    }
}

/**
 * Class mysqlDatabaseDriver
 *
 * @package RBS\Selifa\Data
 */
class mysqlDatabaseDriver extends BaseDatabaseDriver
{
    #region MySQLi Supports
    /**
     * @var array
     */
    private $_Types = array();

    /**
     * @var array
     */
    private $_Flags = array();

    /**
     * @return array
     */
    protected function GetMySQLiTypes()
    {
        $types = array();
        $constants = get_defined_constants(true);
        foreach ($constants['mysqli'] as $c => $n)
        {
            if (preg_match('/^MYSQLI_TYPE_(.*)/', $c, $m))
            {
                $types[$n] = strtolower($m[1]);
            }
        }

        return $types;
    }

    /**
     * @return array
     */
    protected function GetMySQLiFlags()
    {
        $flags = array();
        $constants = get_defined_constants(true);
        foreach ($constants['mysqli'] as $c => $n)
        {
            if (preg_match('/MYSQLI_(.*)_FLAG$/', $c, $m))
            {
                if (!array_key_exists($n, $flags))
                {
                    $flags[$n] = $m[1];
                }
            }
        }

        return $flags;
    }

    /**
     * @param array $typeArray
     * @param int $sType
     * @return string
     */
    protected function MySQLiTypeToDBIType($typeArray, $sType)
    {
        $type = $typeArray[$sType];
        switch ($type)
        {
            case 'char':
            case 'short':
            case 'long':
            case 'int24':
            case 'longlong':
            case 'year':
                return DBI_FT_INTEGER;
            case 'datetime':
            case 'date':
            case 'time':
            case 'newdate':
            case 'interval':
                return DBI_FT_DATETIME;
            case 'timestamp':
                return DBI_FT_TIMESTAMP;
            case 'float':
            case 'double':
            case 'decimal':
            case 'newdecimal':
                return DBI_FT_REAL;
            case 'var_string':
            case 'string':
                return DBI_FT_VARCHAR;
            case 'tiny_blob':
            case 'medium_blob':
            case 'long_blob':
            case 'blob':
                return DBI_FT_BLOB;
        }

        return '';
    }

    /**
     * @param array|string $flags
     * @param int $flagNo
     * @return array
     */
    protected function MySQLiAppliedFlags($flags, $flagNo)
    {
        $result = array();
        foreach ($flags as $n => $t)
        {
            if ($flagNo & $n)
            {
                $result[] = $t;
            }
        }
        return $result;
    }
    #endregion

    public function __construct($dbId, $connectionSpec, $disableError = false)
    {
        parent::__construct($dbId, $connectionSpec, $disableError);
        $hosts = explode(':', $connectionSpec['host'], 2);
        if (count($hosts) > 1)
            $this->ConnectionInstance = @new mysqli($hosts[0], $connectionSpec['user'], $connectionSpec['pass'], $connectionSpec['name'], $hosts[1]);
        else
            $this->ConnectionInstance = @new mysqli($hosts[0], $connectionSpec['user'], $connectionSpec['pass'], $connectionSpec['name']);

        if ($this->ConnectionInstance->connect_error)
        {
            $error = 'Could not connect to MySQL server (' . $connectionSpec['name'] . ') ';
            $error .= 'using `' . $connectionSpec['user'] . '`@`' . $connectionSpec['host'] . '`.';
            $error .= ' Client returned ' . $this->ConnectionInstance->connect_errno . ': ';
            $error .= $this->ConnectionInstance->connect_error;
            $this->_ThrowDBError($error);
        }

        $this->DatabaseName = $connectionSpec['name'];
        $this->DateTimeFormat = 'Y-m-d H:i:s';
        $this->EnclosureStart = '`';
        $this->EnclosureEnd = '`';
        $this->SupportMultiQuery = true;

        $this->Dialect = new MySQLDialect();
        $this->_Types = $this->GetMySQLiTypes();
        $this->_Flags = $this->GetMySQLiFlags();
    }

    #region Overriden Methods
    /**
     * @internal
     * @param string $name
     * @param array $parameters
     * @return mixed
     */
    public function _RawStoredProcedureSQL($name, $parameters)
    {
        if (count($parameters) > 0)
        {
            $s = '';
            foreach ($parameters as $item)
            {
                if ($item === null)
                    $s .= (",NULL");
                else if ($item instanceof DateTime)
                    $s .= (",'".$item->format('Y-m-d H:i:s')."''");
                else if (is_string($item))
                    $s .= (",'".$this->EscapeString($item)."'");
                else
                    $s .= (','.$this->EscapeString($item));
            }
            $s[0] = '(';
        }
        else
            $s = '(';
        return ('CALL '.$name.$s.')');
    }

    /**
     * @internal
     * @param mysqli_result|resource|mixed $result
     * @return mixed
     */
    public function _RawFetch($result)
    {
        return $result->fetch_array(MYSQLI_ASSOC);
    }

    /**
     * @internal
     * @param mysqli_result|resource|mixed $result
     */
    public function _RawFreeResult($result)
    {
        $result->free_result();
    }

    /**
     * @internal
     * @param mysqli_result|resource|mixed $result
     * @return int
     */
    public function _RawGetRowCount($result)
    {
        return $result->num_rows;
    }

    /**
     * @internal
     * @param string $query
     * @return mysqli_result|resource|bool
     */
    public function _RawQuery($query)
    {
        $this->LastQuery = $query;
        if ($this->SupportMultiQuery)
            return $this->ConnectionInstance->multi_query($query);
        else
            return $this->ConnectionInstance->query($query);
    }

    /**
     * @internal
     * @param string $query
     * @return mysqli_result|resource|bool
     */
    public function _RawSingleQuery($query)
    {
        return $this->ConnectionInstance->query($query,MYSQLI_STORE_RESULT);
    }

    /**
     * @internal
     * @param mysqli_result|resource|null $dbResource
     * @param string $tableName
     * @return array
     */
    public function _RawGetResultMetadata($dbResource,$tableName)
    {
        if ($tableName != '')
        {
            $sql = "select * from `".$tableName."` limit 0";
            $dbRes = $this->ConnectionInstance->query($sql,MYSQLI_STORE_RESULT);
            $fields = $dbRes->fetch_fields();
        }
        else
            $fields = $dbResource->fetch_fields();

        $result = array(
            'TableName' => $tableName,
            'ColumnInfo' => array(),
            'IsPrimaryKeyAvailable' => false,
            'PrimaryKeys' => array()
        );

        foreach ($fields as $field)
        {
            $info = array();

            $info['value'] = null;
            $info['name'] = $field->name;
            $info['type'] = $this->MySQLiTypeToDBIType($this->_Types, $field->type);

            $aFlags = $this->MySQLiAppliedFlags($this->_Flags, $field->flags);
            $info['key'] = in_array('PRI_KEY', $aFlags);
            $info['auto'] = in_array('AUTO_INCREMENT', $aFlags);

            if ($info['type'] == DBI_FT_TIMESTAMP)
                $info['auto'] = true;

            $info['modified'] = false;
            $info['forcenull'] = false;

            if ($info['key'])
                $result['PrimaryKeys'][] = $info['name'];

            $result['ColumnInfo'][$field->name] = $info;
        }

        $result['IsPrimaryKeyAvailable'] = (count($result['PrimaryKeys']) > 0);
        return $result;
    }

    /**
     * @param string $input
     * @return string
     */
    public function EscapeString($input)
    {
        return $this->ConnectionInstance->real_escape_string($input);
    }

    /**
     * @param string $message
     * @param int $code
     */
    public function _ThrowDBError($message, $code = DB_GENERAL_ERROR)
    {
        $clientError = $this->ConnectionInstance->error;
        if ($clientError != '')
            $eMessage = ($clientError . ($message != '' ? ' - ' . $message : ''));
        else
            $eMessage = $message;
        parent::_ThrowDBError($eMessage);
    }

    public function _RawLastInsertedIdentifier()
    {
        $q = $this->ConnectionInstance->query('SELECT last_insert_id()');
        $r = $q->fetch_row();
        return $r[0];
    }

    /**
     * @return bool
     */
    public function _RawHasError()
    {
        $clientError = $this->ConnectionInstance->error;
        return ($clientError != '');
    }

    /**
     * @return mysqli_result|resource|null
     */
    public function _RawGetResult()
    {
        return $this->ConnectionInstance->store_result();
    }

    /**
     * @return bool
     */
    public function _RawHasMoreResult()
    {
        return $this->ConnectionInstance->more_results();
    }

    /**
     * @return bool
     */
    public function _RawNextResult()
    {
        return $this->ConnectionInstance->next_result();
    }

    public function _RawTransactionStart()
    {
        $this->ConnectionInstance->begin_transaction();
    }

    public function _RawTransactionSave($id)
    {
        $this->ConnectionInstance->savepoint($id);
    }

    public function _RawTransactionBackTo($id)
    {

    }

    public function _RawTransactionDelete($id)
    {
        $this->ConnectionInstance->release_savepoint($id);
    }

    public function _RawTransactionCommit()
    {
        $this->ConnectionInstance->commit();
    }

    public function _RawTransactionRollback()
    {
        $this->ConnectionInstance->rollback();
    }
    #endregion
}
?>