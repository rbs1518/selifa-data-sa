<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

use \RBS\Selifa\Data\BaseDatabaseDriver;
use \RBS\Selifa\Data\BaseDialect;

/**
 * Class pgsqlDialect
 *
 * @package RBS\Selifa\Data
 */
class pgsqlDialect extends BaseDialect
{
    /**
     *
     */
    function __construct()
    {
        $this->S = '`';
        $this->E = '`';
    }

    /**
     * @param string $sql
     * @param int $offset
     * @param int $count
     * @return string
     */
    public function LimitQuery($sql, $offset, $count)
    {
        if ($offset > 0)
            return ($sql.' limit '.$count.' offset '.$offset);
        else
            return ($sql.' limit '.$count.' offset 0');
    }
}

/**
 * Class pgsqlDatabaseDriver
 *
 * @package RBS\Selifa\Data
 */
class pgsqlDatabaseDriver extends BaseDatabaseDriver
{

    public function __construct($dbId, $connectionSpec, $disableError = false)
    {
        if (!function_exists('pg_connect'))
            throw new Exception('PostgreSQL extension is not installed or enabled.');

        parent::__construct($dbId, $connectionSpec, $disableError);
        $hosts = explode(':', $connectionSpec['host'], 2);
        if (count($hosts) > 1)
            $cString = ("host=".$hosts[0]." port=".$hosts[1]." dbname=".$connectionSpec['name']);
        else
            $cString = ("host=".$hosts[0]." dbname=".$connectionSpec['name']);
        if (isset($connectionSpec['user']))
            if (trim($connectionSpec['user']) != '')
                $cString .= " user=".trim($connectionSpec['user']);
        if (isset($connectionSpec['pass']))
            if (trim($connectionSpec['pass']) != '')
                $cString .= " password=".trim($connectionSpec['pass']);

        $this->ConnectionInstance = @pg_connect($cString,PGSQL_CONNECT_FORCE_NEW);
        if ($this->ConnectionInstance === false)
            $this->_ThrowDBError('Could not connect to PostgreSQL server.');

        $this->DatabaseName = $connectionSpec['name'];
        $this->DateTimeFormat = 'Y-m-d H:i:s';
        $this->EnclosureStart = '';
        $this->EnclosureEnd = '';
        $this->SupportMultiQuery = false;

        $this->Dialect = new pgsqlDialect();
    }

    /**
     * @param string $typeString
     * @return string
     */
    protected function PGTypeToDBIType($typeString)
    {
        $aType = strtolower(trim($typeString));
        if ($aType[0] == '_')
            return DBI_FT_ARRAY;

        switch ($typeString)
        {
            case 'bit':
            case 'boolean':
            case 'bool':
                return DBI_FT_BOOLEAN;
            case 'smallint':
            case 'smallserial':
            case 'integer':
            case 'serial':
            case 'int4':
            case 'int4range':
            case 'bigint':
            case 'bigserial':
            case 'int8':
            case 'int8range':
                return DBI_FT_INTEGER;
            case 'date':
            case 'time':
            case 'time with time zone':
            case 'time without time zone':
            case 'interval':
                return DBI_FT_DATETIME;
            case 'timestamp':
            case 'timestamp without time zone':
            case 'timestamp with time zone':
                return DBI_FT_TIMESTAMP;
            case 'real':
            case 'double precision':
            case 'money':
            case 'decimal':
            case 'numeric':
            case 'numrange':
            case 'float4':
            case 'float8':
                return DBI_FT_REAL;
            case 'character':
            case 'char':
            case 'text':
            case 'varchar':
            case 'character varying':
                return DBI_FT_VARCHAR;
            case 'tiny_blob':
            case 'medium_blob':
            case 'long_blob':
            case 'blob':
                return DBI_FT_BLOB;
            case 'json':
                return DBI_FT_JSON;
            default:
                return DBI_FT_VARCHAR;
        }
    }

    #region Overriden Methods
    /**
     * @internal
     * @param string $name
     * @param array $parameters
     * @return mixed
     */
    public function _RawStoredProcedureSQL($name, $parameters)
    {
        if (count($parameters) > 0)
        {
            $s = '';
            foreach ($parameters as $item)
            {
                if ($item === null)
                    $s .= (",NULL");
                else if ($item instanceof DateTime)
                    $s .= (",'".$item->format('Y-m-d H:i:s')."''");
                else if (is_string($item))
                    $s .= (",'".$this->EscapeString($item)."'");
                else
                    $s .= (','.$this->EscapeString($item));
            }
            $s[0] = '(';
        }
        else
            $s = '(';
        return ('CALL '.$name.$s.')');
    }

    /**
     * @internal
     * @param resource|mixed $result
     * @return mixed
     */
    public function _RawFetch($result)
    {
        $return = pg_fetch_array($result);
        if ($return === false)
            return NULL;
        return $return;
    }

    /**
     * @internal
     * @param resource|mixed $result
     */
    public function _RawFreeResult($result)
    {
        pg_free_result($result);
    }

    /**
     * @internal
     * @param resource|mixed $result
     * @return int
     */
    public function _RawGetRowCount($result)
    {
        return (int)pg_num_rows($result);
    }

    /**
     * @internal
     * @param string $query
     * @return resource|bool
     */
    public function _RawQuery($query)
    {
        $this->LastQuery = $query;
        return pg_query($this->ConnectionInstance,$query);
    }

    /**
     * @internal
     * @param string $query
     * @return resource|bool
     */
    public function _RawSingleQuery($query)
    {
        return pg_query($this->ConnectionInstance,$query);
    }

    /**
     * @internal
     * @param resource $dbResource
     * @param string $tableName
     * @return array
     */
    public function _RawGetResultMetadata($dbResource, $tableName)
    {
        $result = array(
            'TableName' => $tableName,
            'ColumnInfo' => array(),
            'IsPrimaryKeyAvailable' => false,
            'PrimaryKeys' => array()
        );

        if ($tableName != '')
        {
            $sql = "SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type
                    FROM   pg_index i
                    JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                         AND a.attnum = ANY(i.indkey)
                    WHERE  i.indrelid = '".$tableName."'::regclass
                    AND    i.indisprimary";
            $kResult = pg_query($this->ConnectionInstance,$sql);

            $keys = array();
            while ($row = pg_fetch_assoc($kResult))
                $keys[] = trim($row['attname']);

            pg_free_result($kResult);

            $tnSplits = explode('.',$tableName,2);
            if (count($tnSplits) > 1)
            {
                $sql = "SELECT column_name, column_default, is_nullable, data_type, udt_name
                    FROM information_schema.columns
                    WHERE (table_schema = '".$tnSplits[0]."') AND (table_name = '".$tnSplits[1]."')";
            }
            else
            {
                $tName = ($this->EnclosureStart.$tableName.$this->EnclosureEnd);
                $sql = "SELECT column_name, column_default, is_nullable, data_type, udt_name
                    FROM information_schema.columns
                    WHERE (table_name = '".$tName.")";
            }

            $qResult = pg_query($this->ConnectionInstance,$sql);
            while ($row = pg_fetch_assoc($qResult))
            {
                $info = array();
                $fieldName = trim($row['column_name']);
                $fieldType = trim($row['udt_name']);

                $info['value'] = null;
                $info['name'] = $fieldName;
                $info['type_o'] = $fieldType;
                $info['type'] = $this->PGTypeToDBIType($fieldType);

                $info['key'] = (in_array($fieldName,$keys));
                $info['auto'] = (strpos($row['column_default'],'nextval') > -1);

                if ($info['type'] == DBI_FT_TIMESTAMP)
                    $info['auto'] = true;

                $info['modified'] = false;
                $info['forcenull'] = false;

                if ($info['key'])
                    $result['PrimaryKeys'][] = $info['name'];

                $result['ColumnInfo'][$fieldName] = $info;
            }

            pg_free_result($qResult);
        }
        else
        {
            $numFields = pg_num_fields($dbResource);
            for ($i=0;$i<$numFields;$i++)
            {
                $info = array();
                $fieldName = pg_field_name($dbResource,$i);
                $fieldType = pg_field_type($dbResource,$i);

                $info['value'] = null;
                $info['name'] = $fieldName;
                $info['type_o'] = $fieldType;
                $info['type'] = $this->PGTypeToDBIType($fieldType);

                $info['key'] = false;
                $info['auto'] = false;

                if ($info['type'] == DBI_FT_TIMESTAMP)
                    $info['auto'] = true;

                $info['modified'] = false;
                $info['forcenull'] = false;

                if ($info['key'])
                    $result['PrimaryKeys'][] = $info['name'];

                $result['ColumnInfo'][$fieldName] = $info;
            }
        }

        $result['IsPrimaryKeyAvailable'] = (count($result['PrimaryKeys']) > 0);
        return $result;
    }

    /**
     * @param string $input
     * @return string
     */
    public function EscapeString($input)
    {
        return pg_escape_string($this->ConnectionInstance,$input);
    }
    #endregion
}
?>