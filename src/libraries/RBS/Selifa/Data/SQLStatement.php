<?php
namespace RBS\Selifa\Data;

define('DBI_SQL_STATEMENT_RUN',0);
define('DBI_SQL_STATEMENT_GET_DATA',1);

/**
 * Class SQLStatement
 * @package RBS\Selifa\Data
 */
class SQLStatement
{
    /**
     * @var null|BaseDatabaseDriver
     */
    private $_DB = null;

    /**
     * @var string
     */
    private $_SQL = '';

    /**
     * @var int
     */
    private $_Type = DBI_SQL_STATEMENT_GET_DATA;

    /**
     * @var null|array
     */
    private $_Parameters = null;

    /**
     * @var null|array
     */
    private $_TransformationArray = null;

    /**
     * SQLStatement constructor.
     * @param BaseDatabaseDriver $db
     * @param string $sql
     * @param int $sType
     */
    public function __construct($db,$sql='',$sType=DBI_SQL_STATEMENT_GET_DATA)
    {
        $this->_DB = $db;
        $this->_SQL = $sql;
        $this->_Type = DBI_SQL_STATEMENT_GET_DATA;
    }

    /**
     * @return string
     */
    public function GetString()
    {
        return $this->_SQL;
    }

    /**
     * @param string $sql
     */
    public function Prepend($sql)
    {
        $this->_SQL = ($sql.$this->_SQL);
    }

    /**
     * @param string $sql
     */
    public function Append($sql)
    {
        $this->_SQL .= $sql;
    }

    /**
     * @param string $tableAlias
     */
    public function WrapInSelect($tableAlias)
    {
        $this->_SQL = $this->_DB->Dialect->WrapInSelect($this->_SQL,$tableAlias);
    }

    /**
     *
     */
    public function Clear()
    {
        $this->_SQL = '';
    }

    /**
     * @param mixed $value
     */
    public function AddParameter($value)
    {
        if ($this->_Parameters == null)
            $this->_Parameters = array();
        $this->_Parameters[] = $value;
    }

    /**
     * @return null|bool|DataTable[]|DataTable|array
     */
    public function Execute()
    {
        $functionName = 'GetData';
        if ($this->_Type == DBI_SQL_STATEMENT_RUN)
            $functionName = 'Run';

        $dbo = $this->_DB->Prepare($this->_SQL);
        if ($this->_Parameters != null)
            $result = call_user_func_array(array($dbo,$functionName),$this->_Parameters);
        else
            $result = call_user_func(array($dbo,$functionName));
        if ($this->_TransformationArray !== null)
            $result->RegisterTransformation($this->_TransformationArray);
        return $result;
    }

    /**
     * @return null|BaseDialect
     */
    public function GetDialect()
    {
        return $this->_DB->Dialect;
    }

    /**
     * @param $tArray
     */
    public function RegisterTransformationArray($tArray)
    {
        if ($tArray !== null)
            if (count($tArray) > 0)
                $this->_TransformationArray = $tArray;
    }
}
?>