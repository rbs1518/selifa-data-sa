<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa
{
    //Handle compatibility for running this class outside selifa framework.
    if (!defined('SELIFA')) { define('SELIFA_EXCEPTION_SYSTEM_START_CODE',10000); interface IComponent { } }
}

namespace RBS\System
{
    //Handle compatibility for running this class outside selifa framework.
    if (!defined('SELIFA')) { interface ITransferableObject { } }
}

namespace RBS\Selifa\Template
{
    //Handle compatibility for running this class outside selifa framework.
    if (!defined('SELIFA'))
    {
        //If TGX is not used.
        if (!defined('SELIFA_TGX'))
        {
            interface ITGXObjectRenderer { }
            class TGX { }
        }
    }
}

namespace RBS\Selifa\Data
{
    use Exception;
    use RBS\Selifa\IComponent;

    define('SELIFA_DATABASE_INTERFACE', 'v4.1');

    /*Database Interface Constants*/
    define('DBI_FT_INTEGER', 'int');
    define('DBI_FT_REAL', 'real');
    define('DBI_FT_VARCHAR', 'string');
    define('DBI_FT_BLOB', 'blob');
    define('DBI_FT_DATETIME', 'datetime');
    define('DBI_FT_TIMESTAMP', 'timestamp');
    define('DBI_FT_ARRAY','array');
    define('DBI_FT_COMPOSITE','composite');
    define('DBI_FT_RECORD','record');
    define('DBI_FT_ENUM','enum');
    define('DBI_FT_JSON','json');
    define('DBI_FT_BOOLEAN','bool');

    define('DB_DELETE', 'mxqownxuiqwebvryvbiwucnwo');
    define('DB_NULL', 'mcjhsiofkhsusikdjhsnbfryc');

    define('DB_ROW_MODE_INSERT', 88881);
    define('DB_ROW_MODE_UPDATE', 88882);
    define('DB_ROW_MODE_READ_ONLY', 88883);

    define('DML_INSERT', 991);
    define('DML_UPDATE', 992);
    define('DML_DELETE', 993);

    /* Database Error Constants */
    define('DB_INVALID_DATABASE_CONNECTION', SELIFA_EXCEPTION_SYSTEM_START_CODE + 70);
    define('DB_DRIVER_NOT_EXISTS', SELIFA_EXCEPTION_SYSTEM_START_CODE + 71);
    define('DB_INVALID_CONFIGURATION', SELIFA_EXCEPTION_SYSTEM_START_CODE + 72);
    define('DB_GENERAL_ERROR', SELIFA_EXCEPTION_SYSTEM_START_CODE + 73);
    define('DB_CONNECT_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 74);
    define('DB_READ_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 75);
    define('DB_INSERT_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 76);
    define('DB_UPDATE_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 77);
    define('DB_DELETE_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 78);
    define('DB_DDL_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 79);
    define('DB_FOREIGN_KEY_VIOLATED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 80);
    define('DB_KEY_ISNOT_UNIQUE', SELIFA_EXCEPTION_SYSTEM_START_CODE + 81);


    /**
     * Class Database
     * @package RBS\Selifa
     */
    class DBI implements IComponent
    {
        /**
         * @var DBI|null
         */
        private static $_Instance = null;

        /**
         * @var string
         */
        private $_DriverPath = '';

        /**
         * @var string
         */
        private $_DefaultDBName = 'Default';

        /**
         * @var array
         */
        private $_Connections = array();

        /**
         * @var string[]
         */
        private $_LoadedDrivers = array();

        /**
         * @var BaseDatabaseDriver[]
         */
        private $_Database = array();

        /**
         * @var bool
         */
        private $_DisableError = false;

        /**
         * @var null|IDataExtension
         */
        private $_Extension = null;

        /**
         * @param array $options
         */
        private function __construct($options)
        {
            $this->_DriverPath = ($options['DriverPath'].'dbi/');
            if (isset($options['DefaultDatabase']))
                $this->_DefaultDBName = $options['DefaultDatabase'];
            if (isset($options['DisableError']))
                $this->_DisableError = $options['DisableError'];
            if (isset($options['Connections']))
                $this->_Connections = $options['Connections'];
        }

        /**
         * @param string $dbName
         * @return BaseDatabaseDriver
         * @throws DatabaseException
         */
        public function GetDatabaseDriver($dbName)
        {
            if (!isset($this->_Database[$dbName]))
            {
                if (!isset($this->_Connections[$dbName]))
                    throw new DatabaseException(DB_INVALID_DATABASE_CONNECTION,'Invalid database connection name ('.$dbName.').');

                $dbConfig = $this->_Connections[$dbName];
                $dbType = strtolower($dbConfig['type']);
                $driverClassName = ($dbType.'DatabaseDriver');
                if (!in_array($dbType,$this->_LoadedDrivers))
                {
                    $driverFile = ($this->_DriverPath.$dbType.'.php');
                    if (!file_exists($driverFile))
                        throw new DatabaseException(DB_DRIVER_NOT_EXISTS,'Database driver is not exists ('.$dbType.')');
                    require_once($driverFile);
                }
                $this->_Database[$dbName] = new $driverClassName($dbName,$dbConfig,$this->_DisableError);
            }

            $db = $this->_Database[$dbName];
            if ($this->_Extension != null)
                $db->SetExtension($this->_Extension);
            return $db;
        }

        /**
         * @param array $options
         * @return null|DBI
         */
        public static function Initialize($options)
        {
            if (self::$_Instance == null)
                self::$_Instance = new DBI($options);
            return self::$_Instance;
        }

        /**
         * @param string $dbName
         * @return BaseDatabaseDriver
         * @throws DatabaseException
         */
        public static function Get($dbName='')
        {
            $askedDBName = self::$_Instance->_DefaultDBName;
            if ($dbName != '')
                $askedDBName = $dbName;
            return self::$_Instance->GetDatabaseDriver($askedDBName);
        }

        /**
         * @param IDataExtension $dataExtensionObject
         */
        public static function RegisterExtension($dataExtensionObject)
        {
            if (self::$_Instance != null)
                self::$_Instance->_Extension = $dataExtensionObject;
        }

        /**
         * @return null|IDataExtension
         */
        public static function GetRegisteredExtension()
        {
            if (self::$_Instance != null)
                return self::$_Instance->_Extension;
            return null;
        }
    }
}
?>