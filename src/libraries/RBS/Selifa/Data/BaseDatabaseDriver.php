<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;
use Exception;

/**
 * Class BaseDatabaseDriver
 *
 * @package RBS\Selifa\Data
 */
abstract class BaseDatabaseDriver
{
    #region Fields
    /**
     * @var string
     */
    private $_ParameterizedQueryPattern = '#@([0-9]+)#';

    /**
     * @var array
     */
    protected $ConnectionSpecification;

    /**
     * @var string
     */
    public $DatabaseId;

    /**
     * @var bool
     */
    public $IsErrorDisable;

    /**
     * @var string
     */
    public $InterfaceType;

    /**
     * @var resource|bool|null
     */
    protected $ConnectionInstance = false;

    /**
     * @var string
     */
    public  $LastQuery = '';

    /**
     * @var string
     */
    protected $DatabaseName = '';

    /**
     * @var string
     */
    public $DateTimeFormat = '';

    /**
     * @var string
     */
    public $EnclosureStart = '';

    /**
     * @var string
     */
    public $EnclosureEnd = '';

    /**
     * @var bool
     */
    public $SupportMultiQuery = false;

    /**
     * @var bool
     */
    protected $DisableLastQueryOnError = false;

    /**
     * @var array
     */
    private $_QueryParameters = array();

    /**
     * @var null|IDataExtension
     */
    private $_Extension = null;

    /**
     * @var null|BaseDialect
     */
    public $Dialect = null;
    #endregion

    /**
     * @param string $dbId
     * @param array $connectionSpec
     * @param bool $disableError
     */
    public function __construct($dbId, $connectionSpec, $disableError = false)
    {
        $this->ConnectionSpecification = $connectionSpec;
        $this->InterfaceType = $connectionSpec['type'];
        $this->DatabaseId = $dbId;
        $this->IsErrorDisable = $disableError;
    }

    #region Internal Use Only
    /**
     * @internal
     * @param int $fieldType
     * @return bool
     */
    public function _IsStringType($fieldType)
    {
        return (($fieldType == DBI_FT_VARCHAR) ||
            ($fieldType == DBI_FT_DATETIME) ||
            ($fieldType == DBI_FT_BLOB));
    }

    /**
     * @internal
     * @param string $message
     * @param int $code
     * @throws DatabaseException
     */
    public function _ThrowDBError($message,$code=DB_GENERAL_ERROR)
    {
        if (!$this->IsErrorDisable)
        {
            if ($this->DisableLastQueryOnError)
                $msg = 'DB Error: '.$message;
            else
                $msg = 'DB Error: ' . $message . ($this->LastQuery != '' ? ', Last Query: ' . $this->LastQuery : '');
            throw new DatabaseException($code,$msg);
        }
    }

    /**
     * @param string $key
     * @return string
     */
    private function __queryParseReplace($key)
    {
        $aKey = ((int)$key[1] - 1);
        if (isset($this->_QueryParameters[$aKey]))
        {
            $var = $this->_QueryParameters[$aKey];
            if (is_string($var))
                return ("'".$this->EscapeString($var)."'");
            else if (is_bool($var))
                return ($var?'1':'0');
            else if (is_array($var))
            {
                $s = '';
                foreach ($var as $item)
                {
                    if (is_string($item))
                        $s .= (",'" . $this->EscapeString($item) . "'");
                    else
                        $s .= (',' . $this->EscapeString($item));
                }
                $s[0] = '(';
                return ($s.')');
                //return $this->EscapeString($s.')');
            }
            else
                return $this->EscapeString($var);
        }

        return '';
    }

    /**
     * @internal
     * @param string $queryString
     * @param array $parameters
     * @return string
     */
    public function _ParseQuery($queryString,$parameters=array())
    {
        $argCount = count($parameters);
        if ($argCount > 0)
        {
            $this->_QueryParameters = $parameters;
            return preg_replace_callback(
                $this->_ParameterizedQueryPattern, array($this, '__queryParseReplace'), $queryString);
        }
        return $queryString;
    }
    #endregion

    #region Abstract Functions
    /**
     * @internal
     * @param string $name
     * @param array $parameters
     * @return mixed
     */
    abstract public function _RawStoredProcedureSQL($name,$parameters);

    /**
     * @internal
     * @param resource|mixed $result
     * @return mixed
     */
    abstract public function _RawFetch($result);

    /**
     * @internal
     * @param resource|mixed $result
     */
    abstract public function _RawFreeResult($result);

    /**
     * @internal
     * @param resource|mixed $result
     * @return int
     */
    abstract public function _RawGetRowCount($result);

    /**
     * @internal
     * @param string $query
     * @return resource|bool
     */
    abstract public function _RawQuery($query);

    /**
     * @internal
     * @param string $query
     * @return resource|bool
     */
    abstract public function _RawSingleQuery($query);

    /**
     * @internal
     * @param resource $dbResource
     * @param string $tableName
     * @return array
     */
    abstract public function _RawGetResultMetadata($dbResource,$tableName);

    /**
     * @param string $input
     * @return string
     */
    abstract public function EscapeString($input);
    #endregion

    #region Virtual Functions
    /**
     * @internal
     * @return string|float|int|bool
     */
    public function _RawLastInsertedIdentifier() { return 0; }

    /**
     * @return bool
     */
    public function _RawHasError(){ return false; }

    /**
     * @return mixed|null
     */
    public function _RawGetResult(){ return null; }

    /**
     * @internal
     * @return bool
     */
    public function _RawHasMoreResult(){ return false; }

    /**
     * @internal
     */
    public function _RawNextResult() { }

    /**
     * @internal
     */
    public function _RawTransactionStart() { }

    /**
     * @internal
     * @param string $id
     */
    public function _RawTransactionSave($id) { }

    /**
     * @internal
     * @param string $id
     */
    public function _RawTransactionBackTo($id) { }

    /**
     * @internal
     * @param string $id
     */
    public function _RawTransactionDelete($id) { }

    /**
     * @internal
     */
    public function _RawTransactionCommit() { }

    /**
     * @internal
     */
    public function _RawTransactionRollback() { }
    #endregion

    /**
     * @param IDataExtension $iExtension
     */
    public function SetExtension(IDataExtension $iExtension)
    {
        $this->_Extension = $iExtension;
    }

    /**
     * @param string $sqlCommand
     * @return DataObject
     */
    public function Prepare($sqlCommand)
    {
        return new DataObject($this,$sqlCommand,false,false,$this->_Extension);
    }

    /**
     * @param string $tableName
     * @return DataObject
     */
    public function GetTable($tableName)
    {
        return new DataObject($this,'',true,false,$tableName,$this->_Extension);
    }

    /**
     * @param string $procedureName
     * @return DataObject
     */
    public function UseProcedure($procedureName)
    {
        return new DataObject($this,$procedureName,false,true,$this->_Extension);
    }

    /**
     *
     */
    public function BeginTransaction()
    {
        $this->_RawTransactionStart();
    }

    /**
     * @param bool $cancel
     */
    public function EndTransaction($cancel=false)
    {
        if ($cancel)
            $this->_RawTransactionRollback();
        else
            $this->_RawTransactionCommit();
    }

    /**
     * @param string $sql
     * @param int $type
     * @return SQLStatement
     */
    public function PrepareStatement($sql='',$type=1)
    {
        return new SQLStatement($this,$sql,$type);
    }
}
?>