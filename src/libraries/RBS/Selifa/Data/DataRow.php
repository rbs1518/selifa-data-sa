<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;
use DateTime;
use RBS\Selifa\Template\ITGXObjectRenderer;
use RBS\Selifa\Template\TGX;
use RBS\System\ITransferableObject;
use JsonSerializable;

/**
 * Class DataRow
 *
 * @package RBS\Selifa\Data
 */
class DataRow implements ITransferableObject, ITGXObjectRenderer, JsonSerializable
{
    /**
     * @var null|BaseDatabaseDriver
     */
    private $_Driver = null;

    /**
     * @var null|IDataExtension
     */
    private $_Extension = null;

    /**
     * @var null|array
     */
    private $_MetaData = null;

    /**
     * @var int
     */
    private $_RowMode;

    /**
     * @var string
     */
    public $LastDMLQuery = '';

    /**
     * @var null|DataTable
     */
    private $_SourceDT = null;

    /**
     * @param BaseDatabaseDriver $driverObject
     * @param array $rowValues
     * @param array $metaData
     * @param int $rowMode
     * @param IDataExtension $iExtension
     * @param DataTable $sourceDT
     */
    public function __construct($driverObject,$rowValues,$metaData,$rowMode,$iExtension=null,$sourceDT=null)
    {
        $this->_Driver = $driverObject;
        $this->_RowMode = $rowMode;
        $this->_MetaData = $metaData;
        $this->_Extension = $iExtension;
        $this->_SourceDT = $sourceDT;

        if ($rowValues != null)
        {
            foreach ($rowValues as $key => $value )
            {
                if (isset($this->_MetaData['ColumnInfo'][$key]))
                    $this->_MetaData['ColumnInfo'][$key]['value'] = $value;
            }
        }
    }

    /**
     * @return array
     */
    function GetColumnNames()
    {
        $result = array();
        foreach ($this->_MetaData['ColumnInfo'] as $key => $content)
            $result[] = $key;
        return $result;
    }

    /**
     * @param string $name
     * @return string|float|int|bool|null
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->_MetaData['ColumnInfo']))
        {
            if ($this->_SourceDT != null)
            {
                $value = $this->_MetaData['ColumnInfo'][$name]['value'];
                $tObject = $this->_SourceDT->GetTransformation($name);
                if ($tObject != null)
                    return $tObject->ConvertTo($value);
                else
                    return $value;
            }
            else
                return $this->_MetaData['ColumnInfo'][$name]['value'];
        }
        return null;
    }

    /**
     * @param string $name
     * @param string|float|int|bool|null $value
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_MetaData['ColumnInfo']))
        {
            if ($this->_SourceDT != null)
            {
                $tObject = $this->_SourceDT->GetTransformation($name);
                if ($tObject != null)
                    $aValue = $tObject->ConvertFrom($value);
                else
                    $aValue = $value;
            }
            else $aValue = $value;

            if (($this->_MetaData['ColumnInfo'][$name]['value'] != $aValue) || ($aValue == ''))
            {
                if (($aValue !== DB_NULL) && ($this->_MetaData['ColumnInfo'][$name]['type'] != DBI_FT_TIMESTAMP))
                {
                    $this->_MetaData['ColumnInfo'][$name]['value'] = $aValue;
                    $this->_MetaData['ColumnInfo'][$name]['modified'] = true;
                }
                else
                    $this->_MetaData['ColumnInfo'][$name]['forcenull'] = true;
            }
        }
    }

    /**
     * @return string|float|int|bool|null
     */
    function FirstColumnValue()
    {
        $temp = array_keys($this->_MetaData['ColumnInfo']);
        if (count($temp) > 0)
        {
            if ($this->_SourceDT != null)
            {
                $value = $this->Columns[$temp[0]]['value'];
                $tObject = $this->_SourceDT->GetTransformation($temp[0]);
                if ($tObject != null)
                    return $tObject->ConvertTo($value);
                else
                    return $value;
            }
            else
                return $this->Columns[$temp[0]]['value'];
        }
        else
            return null;
    }

    /**
     * @return bool
     * @throws DatabaseException
     */
    public function Save()
    {
        if ($this->_RowMode == DB_ROW_MODE_READ_ONLY)
        {
            $this->_Driver->_ThrowDBError('Could not save a read only row.');
            return false;
        }

        $cancelSave = false;
        if ($this->_Extension != null)
            $this->_Extension->SaveRow($this,$this->_RowMode,$cancelSave);
        if ($cancelSave)
            return false;

        $pairs = array();
        $validInput = true;

        $invalidColumnName = '';
        $invalidColumnData = '';

        foreach ($this->_MetaData['ColumnInfo'] as $key => $value)
        {
            $vType = $value['type'];
            $notSkipped = (!$value['auto']) && ($vType != DBI_FT_TIMESTAMP) && (!$value['forcenull']);
            $isModified = true;
            if (isset($value['modified']))
                $isModified = (bool)$value['modified'];
            $isModified = ($isModified || ($this->_RowMode == DB_ROW_MODE_INSERT));
            if ($notSkipped && $isModified)
            {
                if ($value['value'] === null)
                {
                    //$pairs[$key] = null;
                }
                else
                {
                    if (($vType == DBI_FT_INTEGER) || ($vType == DBI_FT_REAL))
                    {
                        $validInput = is_numeric($value['value']);
                        $pairs[$key] = $value['value'];
                        if (!$validInput)
                        {
                            $invalidColumnName = $key;
                            $invalidColumnData = $value['value'];
                        }
                    }
                    else
                    {
                        if ($vType == DBI_FT_DATETIME)
                        {
                            if ($value['value'] instanceof DateTime)
                                $pairs[$key] = $value['value']->format($this->_Driver->DateTimeFormat);
                            else
                            {
                                if (is_string($value['value']))
                                    $pairs[$key] = $this->_Driver->EscapeString($value['value']);
                                else
                                {
                                    if (is_integer($value['value']))
                                    {
                                        $dtObject = new DateTime();
                                        $dtObject->setTimestamp($value['value']);
                                        $pairs[$key] = $dtObject->format($this->_Driver->DateTimeFormat);
                                    }
                                    else
                                    {
                                        $validInput = false;
                                        $invalidColumnName = $key;
                                        $invalidColumnData = $value['value'];
                                    }
                                }
                            }
                        }
                        else
                            $pairs[$key] = (string)$this->_Driver->EscapeString($value['value']);
                    }
                }
            }
            if (!$validInput)
                break;
        }

        if (!$validInput)
        {
            $this->_Driver->_ThrowDBError('Invalid input value for ' . $invalidColumnName . ', value is: ' . $invalidColumnData . '.');
            return false;
        }

        if (count($pairs) <= 0)
            return true;

        $d = $this->_Driver->Dialect;
        if ($this->_RowMode == DB_ROW_MODE_INSERT)
        {
            $partA = '';
            $partB = '';
            foreach ($pairs as $cn => $cv)
            {
                $partA .= (','.$d->S.$cn.$d->E);
                if (is_string($cv))
                    $partB .= (",'".$cv."'");
                else
                    $partB .= (",".$cv);
            }

            $partA[0] = '(';
            $partB[0] = '(';
            $sql = ('INSERT INTO '.$d->S.$this->_MetaData['TableName'].$d->E.$partA.') VALUES'.$partB.')');
            $this->LastDMLQuery = $sql;
            $b = $this->_Driver->_RawSingleQuery($sql);
            if (!$b)
            {
                $this->_Driver->_ThrowDBError('Insert query failed.', DB_INSERT_FAILED);
                return false;
            }

            if ($this->_MetaData['IsPrimaryKeyAvailable'])
            {
                $pKey = $this->_MetaData['PrimaryKeys'][0];
                if ($this->_MetaData['ColumnInfo'][$pKey]['auto'])
                {
                    $lastId = $this->_Driver->_RawLastInsertedIdentifier();
                    $this->_MetaData['ColumnInfo'][$pKey]['value'] = $lastId;
                }
            }

            if ($this->_Extension != null)
                $this->_Extension->RowSaved($this,$this->_RowMode);
            return $b;
        }
        else
        {
            if (!$this->_MetaData['IsPrimaryKeyAvailable'])
            {
                $this->_Driver->_ThrowDBError('No primary key defined.');
                return false;
            }

            $keyColumn = $this->_MetaData['PrimaryKeys'][0];
            $keyValue = $this->_MetaData['ColumnInfo'][$keyColumn]['value'];

            $sPart = '';
            foreach ($pairs as $cn => $cv)
                $sPart .= (','.$d->S.$cn.$d->E.' = '.(is_string($cv)?"'".$cv."'":$cv));
            $sPart[0] = ' ';

            $sql = ('UPDATE '.$d->S.$this->_MetaData['TableName'].$d->E.' SET'.$sPart.' WHERE ('.$d->S.$keyColumn.$d->E.' = '.(is_string($keyValue)?"'".$keyValue."'":$keyValue).')');
            $this->LastDMLQuery = $sql;
            $b = $this->_Driver->_RawSingleQuery($sql);
            if (!$b)
            {
                $this->_Driver->_ThrowDBError('Update query failed.',DB_UPDATE_FAILED);
                return false;
            }

            if ($this->_Extension != null)
                $this->_Extension->RowSaved($this,$this->_RowMode);
            return $b;
        }
    }

    /**
     * @return bool
     * @throws DatabaseException
     */
    public function Delete()
    {
        if ($this->_RowMode == DB_ROW_MODE_READ_ONLY)
        {
            $this->_Driver->_ThrowDBError('Could not save a read only row.');
            return false;
        }

        if (!$this->_MetaData['IsPrimaryKeyAvailable'])
        {
            $this->_Driver->_ThrowDBError('No primary key defined.');
            return false;
        }

        $cancelDelete = false;
        $doUpdateInstead = false;
        if ($this->_Extension != null)
            $this->_Extension->DeleteRow($this,$cancelDelete,$doUpdateInstead);
        if ($cancelDelete)
            return false;

        if ($doUpdateInstead)
        {
            return $this->Save();
        }
        else
        {
            $keyColumn = $this->_MetaData['PrimaryKeys'][0];
            $keyValue = $this->_MetaData['ColumnInfo'][$keyColumn]['value'];

            $d = $this->_Driver->Dialect;
            $sql = ('DELETE FROM ' . $d->S . $this->_MetaData['TableName'] . $d->E . ' WHERE (' . $d->S . $keyColumn . $d->E . ' = ' . (is_string(
                    $keyValue) ? "'" . $keyValue . "'" : $keyValue) . ')');
            $this->LastDMLQuery = $sql;
            $b = $this->_Driver->_RawSingleQuery($sql);
            if (!$b)
                $this->_Driver->_ThrowDBError('Delete query failed.', DB_UPDATE_FAILED);

            if ($this->_Extension != null)
                $this->_Extension->RowDeleted($this);
            return $b;
        }
    }

    #region ITransferableObject implementation(s)
    /**
     * @return array
     */
    public function GetTransferableMembers()
    {
        $check = array(DBI_FT_BLOB,DBI_FT_DATETIME,DBI_FT_TIMESTAMP,DBI_FT_VARCHAR);
        $result = array();
        foreach ($this->_MetaData['ColumnInfo'] as $key => $item)
        {
            $result[] = array(
                'Name' => $key,
                'DataType' => (in_array($item['type'],$check)?'string':$item['type'])
            );
        }
        return $result;
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     */
    public function To(&$tObjectOrArray)
    {
        if ($tObjectOrArray instanceof ITransferableObject)
        {
            $tMembers = $tObjectOrArray->GetTransferableMembers();
            foreach ($tMembers as $m)
            {
                $value = $this->{$m['Name']};
                if ($m['DataType'] == 'bool')
                    $tObjectOrArray->{$m['Name']} = ($value == '1');
                else if ($m['DataType'] == 'int')
                    $tObjectOrArray->{$m['Name']} = (int)$value;
                else if ($m['DataType'] == 'float')
                    $tObjectOrArray->{$m['Name']} = (float)$value;
                else if ($m['DataType'] == 'double')
                    $tObjectOrArray->{$m['Name']} = (double)$value;
                else
                    $tObjectOrArray->{$m['Name']} = $value;
            }
        }
        else if (is_array($tObjectOrArray))
        {
            foreach ($this->_MetaData['ColumnInfo'] as $key => $item)
                $tObjectOrArray[$key] = $this->{$key}; //$tObjectOrArray[$key] = $item['value'];
        }
    }

    /**
     * @return array
     */
    public function ToArray()
    {
        $res = array();
        foreach ($this->_MetaData['ColumnInfo'] as $key => $val)
            $res[$key] = $this->{$key}; //$res[$key] = $val['value'];
        return $res;
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     */
    public function From($tObjectOrArray)
    {
        if ($tObjectOrArray instanceof ITransferableObject)
        {
            $tMembers = $tObjectOrArray->GetTransferableMembers();
            foreach ($tMembers as $m)
            {
                $value = $tObjectOrArray->{$m['Name']};
                if (is_bool($value))
                    $this->{$m['Name']} = ($value?1:0);
                else
                    $this->{$m['Name']} = $value;
            }
        }
        else if (is_array($tObjectOrArray))
        {
            foreach ($tObjectOrArray as $key => $value)
            {
                if (is_bool($value))
                    $this->{$key} = ($value?1:0);
                else
                    $this->{$key} = $value;
            }
        }
    }
    #endregion

    #region ITGXObjectRenderer implementation
    /**
     * @param TGX $tgxObject
     * @return mixed|array
     */
    public function TGXRender($tgxObject)
    {
        return $this->ToArray();
    }
    #endregion

    #region JsonSerializable implementation
    /**
     *
     */
    public function jsonSerialize()
    {
        return $this->ToArray();
    }
    #endregion
}
?>