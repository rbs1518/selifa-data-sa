<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;

/**
 * Class Model
 * @package RBS\Selifa\Data
 */
abstract class Model
{
    /**
     * @var null|BaseDatabaseDriver
     */
    protected $Driver = null;

    /**
     * @var null|DataRow
     */
    protected $RowData = null;

    /**
     * @var null|string|int
     */
    protected $KeyIdentifier = null;

    /**
     * @var null|string|int
     */
    protected $CurrentUserID = null;

    /**
     * @var null|DataObject
     */
    protected $Primary = null;

    /**
     * @var array|Model[]
     */
    protected $LinkedItemCache = array();

    /**
     * @param string|int $keyIdentifier
     * @return null|DataRow
     */
    protected function GetRowData($keyIdentifier)
    {
        return null;
    }

    /**
     * @param string $name
     * @return mixed|Model|null
     */
    protected function GetLinkedItem($name)
    {
        return null;
    }

    /**
     * @param string $dbName
     * @param string $primaryTableName
     * @param null|string|int $keyIdentifier
     * @param null|string|int $cUserId
     * @throws DatabaseException
     */
    protected function __construct($dbName='',$primaryTableName='',$keyIdentifier=null,$cUserId=null)
    {
        $this->Driver = DBI::Get($dbName);
        $this->KeyIdentifier = $keyIdentifier;
        $this->CurrentUserID = $cUserId;

        if ($keyIdentifier != null)
            $this->GetRowData($keyIdentifier);
        if ($primaryTableName != '')
            $this->Primary = $this->Driver->GetTable($primaryTableName);
    }

    public function __get($name)
    {
        if (!isset($this->LinkedItemCache[$name]))
        {
            $itemObject = $this->GetLinkedItem($name);
            if ($itemObject == null)
                return null;
            $this->LinkedItemCache[$name] = $itemObject;
        }
        return $this->LinkedItemCache[$name];
    }

    /**
     * @param $sqlCommand
     * @return DataObject
     */
    protected function Prepare($sqlCommand)
    {
        return $this->Driver->Prepare($sqlCommand);
    }

    /**
     * @param string $tableName
     * @return DataObject
     */
    protected function GetTable($tableName)
    {
        return $this->Driver->GetTable($tableName);
    }

    /**
     * @param string $storedProcedureName
     * @return DataObject
     */
    protected function UseProcedure($storedProcedureName)
    {
        return $this->Driver->UseProcedure($storedProcedureName);
    }
}
?>