<?php
namespace RBS\Selifa\Data;
use Exception;

if (!defined('SELIFA')) { interface ISelifaException { } }

class DatabaseException extends Exception implements ISelifaException
{
    /**
     * @var array
     */
    public $ArrayData;

    /**
     * @param int $eCode
     * @param string $message
     * @param array $arrayData
     * @param Exception|null $previousException
     */
    public function __construct($eCode, $message = '', $arrayData = array(), $previousException = null)
    {
        parent::__construct($message, $eCode, $previousException);
        $this->ArrayData = $arrayData;
    }

    /**
     * @param array $aData
     */
    public function AppendData($aData)
    {
        $this->ArrayData = array_merge_recursive($this->ArrayData,$aData);
    }
}
?>