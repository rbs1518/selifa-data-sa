<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\DataTables;
use RBS\Selifa\Data\BaseDatabaseDriver;
use RBS\Selifa\Data\DataTable;
use RBS\Selifa\Data\SQL;
use Exception;
use RBS\Selifa\Data\SQLStatement;
use RBS\Selifa\Data\DBI;

/**
 * Class Parameter
 * @package RBS\Selifa\Data\DataTables
 */
class Parameter
{
    /**
     * @var null|BaseDatabaseDriver
     */
    protected $Driver = null;

    /**
     * @var string
     */
    public $Echo = '';

    /**
     * @var int
     */
    public $DisplayStart = 0;

    /**
     * @var int
     */
    public $DisplayLength = 0;

    /**
     * @var int
     */
    public $ColumnCount = 0;

    /**
     * @var mixed|null|string
     */
    public $SearchText = '';

    /**
     * @var bool
     */
    public $IsRegexSearch = false;

    /**
     * @var int
     */
    public $SortCount = 0;

    /**
     * @var ColumnInfo[]
     */
    public $Columns = array();

    /**
     * @var SortInfo[]
     */
    public $Sorts = array();

    /**
     * @var null|array
     */
    public $FilterSpec = null;

    /**
     * @var null
     */
    public $ExtendedFilters = null;

    /**
     * @var bool
     */
    public $UseExtendedFilters = false;

    /**
     * @var null|SQLStatement
     */
    public $DataStatement = null;

    /**
     * @var null|SQLStatement
     */
    public $FilteredCountStatement = null;

    /**
     * @var null|SQLStatement
     */
    public $TotalCountStatement = null;

    /**
     * @param array $data
     * @param string $keyName
     * @param mixed|null $default
     * @return mixed|null
     */
    protected function Extract($data, $keyName, $default = null)
    {
        if (isset($data[$keyName]))
            return $data[$keyName];
        return $default;
    }

    /**
     * @return array|mixed
     */
    protected function GetDataFromRequest()
    {
        $useJSON = false;

        $contentType = strtolower(trim($_SERVER['CONTENT_TYPE']));
        if (($contentType == 'text/json') || ($contentType == 'application/json'))
            $useJSON = true;

        if ($useJSON)
            return json_decode(file_get_contents('php://input'),true);
        else
        {
            $rMethod = strtoupper(trim($_SERVER['REQUEST_METHOD']));
            if (($rMethod == 'PUT') || ($rMethod == 'PATCH'))
            {
                $var = file_get_contents('php://input');
                $theData = array();
                parse_str($var,$theData);
                return $theData;
            }
            else
                return $_POST;
        }
    }

    /**
     * @param array $specs
     * @param null|array $filters
     * @param string $logic
     * @return string
     * @throws Exception
     */
    protected function SimpleFilterArrayToSQL($specs,$filters=null,$logic='AND')
    {
        $whereSQL = '';
        if ($filters != null)
        {
            if (count($filters) > 0)
            {
                if ($this->Driver === null)
                    throw new Exception('No database driver loaded.');

                foreach ($filters as $key => $value)
                {
                    if (!isset($specs[$key]))
                        continue;
                    if(strpos(strtolower($key), 'date') !== false && is_array($value))
                        //TODO use dialect object to replace "between" keyword.
                        $whereSQL .= (' '.$logic.' ('. $key .' BETWEEN ("'. $value[0] .' 00:00:00") AND ("'. $value[1] .' 23:59:59"))');
                    else
                    {
                        $spec = $specs[$key];
                        $aValue = $this->Driver->EscapeString($value);
                        if (isset($spec[2]))
                            $aValue = ($spec[2].$aValue);
                        if (isset($spec[3]))
                            $aValue .= $spec[3];
                        $whereSQL .= (' '.$logic.' ('.$spec[0].' '.strtoupper($spec[1])." '".$aValue."')");
                    }
                }
            }
        }
        return $whereSQL;
    }

    /**
     * Parameter constructor.
     *
     * @param null|array $dSource
     * @param string $dbDriverName
     * @throws Exception
     */
    public function __construct($dSource=null,$dbDriverName='')
    {
        if ($dSource === null)
            $dSource = $this->GetDataFromRequest();
        $this->Driver = DBI::Get($dbDriverName);

        $this->Echo = $this->Extract($dSource, 'sEcho', '');
        $this->ColumnCount = (int)$this->Extract($dSource, 'iColumns', 0);
        $this->DisplayStart = (int)$this->Extract($dSource, 'iDisplayStart', 0);

        $iDisplayLength = $this->Extract($dSource, 'iDisplayLength', '0');
        if ($iDisplayLength == 'all')
            $this->DisplayLength = 0;
        else
            $this->DisplayLength = (int)$iDisplayLength;

        $this->SearchText = $this->Extract($dSource, 'aSearch', $this->Extract($dSource,'sSearch',''));
        $this->IsRegexSearch = (boolean)$this->Extract($dSource, 'bRegex', false);
        $this->SortCount = (int)$this->Extract($dSource, 'iSortingCols', 0);

        for ($i = 0; $i < $this->ColumnCount; $i++)
            $this->Columns[] = new ColumnInfo($dSource, $i);

        for ($i = 0; $i < $this->SortCount; $i++)
            $this->Sorts[] = new SortInfo($dSource, $i);

        if (isset($dSource['ExtFilters']))
            $this->ExtendedFilters = $dSource['ExtFilters'];
        else if (isset($dSource['AdvFilters'])) //for backward compatibility with pre-2019 dtx version.
            $this->ExtendedFilters = $dSource['AdvFilters'];
        $this->UseExtendedFilters = ($this->ExtendedFilters !== null);
    }

    /**
     * @param array $aData
     * @param int $totalCount
     * @param int|null $filteredCount
     * @return array
     */
    public function GenerateResult($aData,$totalCount,$filteredCount=null)
    {
        if ($filteredCount === null)
            $filteredCount = $totalCount;

        return array(
            'sEcho' => $this->Echo,
            'iTotalRecords' => $totalCount,
            'iTotalDisplayRecords' => $filteredCount,
            'aaData' => $aData,
            'DisplayLength' => $this->DisplayLength,
            'DisplayStart' => $this->DisplayStart
        );
    }

    /**
     * @param SQLStatement|string $dataStatement
     * @return null|SQLStatement
     * @throws Exception
     */
    public function SetDataStatement($dataStatement)
    {
        if ($dataStatement instanceof SQLStatement)
            $this->DataStatement = $dataStatement;
        else if (is_string($dataStatement))
        {
            if ($this->Driver === null)
                throw new Exception('No database driver loaded.');
            $this->DataStatement = new SQLStatement($this->Driver,$dataStatement);

            $aArgs = func_get_args();
            for ($i=1;$i<count($aArgs);$i++)
                $this->DataStatement->AddParameter($aArgs[$i]);
        }
        else
            throw new Exception('Data statement is not an instance of SQLStatement or a string.');
        return $this->DataStatement;
    }

    /**
     * @param SQLStatement|string $fCountStatement
     * @return null|SQLStatement
     * @throws Exception
     */
    public function SetFilteredCountStatement($fCountStatement)
    {
        if ($fCountStatement instanceof SQLStatement)
            $this->FilteredCountStatement = $fCountStatement;
        else if (is_string($fCountStatement))
        {
            if ($this->Driver === null)
                throw new Exception('No database driver loaded.');
            $this->FilteredCountStatement = new SQLStatement($this->Driver,$fCountStatement);

            $aArgs = func_get_args();
            for ($i=1;$i<count($aArgs);$i++)
                $this->FilteredCountStatement->AddParameter($aArgs[$i]);
        }
        else
            throw new Exception('Filtered count statement is not an instance of SQLStatement or a string.');
        return $this->FilteredCountStatement;
    }

    /**
     * @param SQLStatement|string $tCountStatement
     * @return null|SQLStatement
     * @throws Exception
     */
    public function SetTotalCountStatement($tCountStatement)
    {
        if ($tCountStatement instanceof SQLStatement)
            $this->TotalCountStatement = $tCountStatement;
        else if (is_string($tCountStatement))
        {
            if ($this->Driver === null)
                throw new Exception('No database driver loaded.');
            $this->TotalCountStatement = new SQLStatement($this->Driver,$tCountStatement);

            $aArgs = func_get_args();
            for ($i=1;$i<count($aArgs);$i++)
                $this->TotalCountStatement->AddParameter($aArgs[$i]);
        }
        else
            throw new Exception('Data statement is not an instance of SQLStatement or a string.');
        return $this->TotalCountStatement;
    }

    /**
     * Set filter specification for non-extended filter.
     *
     * Specification example:
     *  $specs = array(
     *      'Param1' => array('a.Name','LIKE','%','%'),
     *      'Param2' => array('a.Code','=')
     *   );
     *
     * Filter array example:
     * $filters = array(
     *      'Param1' => 'this is the value',
     *      'Param2' => 90
     * );
     *
     * @param array $spec
     */
    public function SetFilterSpec($spec)
    {
        $this->FilterSpec = $spec;
    }

    /**
     * @param callable $callback
     * @param null|callable $fcCallback
     * @param null|callable $tcCallback
     * @param array $aOutput
     * @return array|null
     * @throws Exception
     */
    public function ProcessWithAdvancedFilters($callback,$fcCallback=null,$tcCallback=null,&$aOutput=array())
    {
        if ($this->ExtendedFilters === null)
            throw new Exception('Parameter(s) or data source does not contains any extended filter(s).');

        $filters = array();
        $orders = array();
        $ctCols = array();
        foreach ($this->ExtendedFilters as $item)
        {
            $fKey = trim($item['filterKey']);
            if ($item['nullFilter'] != 'true')
            {
                $op = strtolower(trim($item['operator']));
                $filters[] = array(
                    'field' => ($fKey!=''?$fKey:trim($item['targetKey'])),
                    'operator' => ($op!=''?$op:'contains'),
                    'value' => $item['filter']
                );
            }

            if ($fKey != '')
            {
                if ($item['sort'] != '')
                {
                    $orderNo = (int)$item['sortOrder'];
                    if ($orderNo > 0)
                        $orders[$orderNo-1] = array(
                            'col' => $fKey,
                            'dir' => strtoupper(trim($item['sort']))
                        );
                    else
                        array_push($orders,array(
                            'col' => $fKey,
                            'dir' => strtoupper(trim($item['sort']))
                        ));
                }
            }
            else
            {
                if ($item['sort'] != '')
                {
                    $orderNo = (int)$item['sortOrder'];
                    if ($orderNo > 0)
                        $orders[$orderNo-1] = array(
                            'col' => $item['targetKey'],
                            'dir' => strtoupper(trim($item['sort']))
                        );
                    else
                        array_push($orders,array(
                            'col' => $item['targetKey'],
                            'dir' => strtoupper(trim($item['sort']))
                        ));
                }
            }

            if ($item['isCustom'] == 'true')
                $ctCols[$item['key']] = $item['targetKey'];
        }

        if (!is_callable($callback))
            return null;

        $data = $callback($filters,$orders,$ctCols,$aOutput);
        if ($data instanceof DataTable)
            $data = $data->ToArray(true);

        $tCount = 0;
        if (is_callable($tcCallback))
            $tCount = $tcCallback($aOutput);
        if ($tCount < 0)
            $tCount = count($data);

        if ($this->DisplayLength <= 0)
            $fCount = count($data);
        else
        {
            if (is_callable($fcCallback))
                $fCount = $fcCallback($filters,$aOutput);
            else
                $fCount = $tCount;
        }

        $aOutput['sEcho'] = $this->Echo;
        $aOutput['iTotalRecords'] = ($tCount>0?$tCount:$fCount);
        $aOutput['iTotalDisplayRecords'] = $fCount;
        $aOutput['aaData'] = $data;
        $aOutput['DisplayLength'] = $this->DisplayLength;
        $aOutput['DisplayStart'] = $this->DisplayStart;

        return $aOutput;
    }

    /**
     * @param string $tablePrefix
     * @return array|null
     * @throws Exception
     */
    public function Process($tablePrefix='z')
    {
        if (!$this->UseExtendedFilters)
        {
            //no extended filters



        }
        else
        {
            //with extended filters
            return $this->ProcessWithAdvancedFilters(function($fs,$os,$ct,&$out) use(&$tablePrefix)
            {
                $_dialect = $this->DataStatement->GetDialect();

                $sql = '';
                if ($fs != null)
                {
                    if (count($fs) <= 0)
                        $fs = null;
                    else
                        $fs = array(
                            'condition' => 'AND',
                            'rules' => $fs
                        );
                    $converter = new QBConverter($fs,$_dialect);
                    $sql .= (" ".$converter->Convert($tablePrefix,true));
                }
                if (count($os) > 0)
                {
                    $qOrders = array();
                    for ($i=0;$i<count($os);$i++)
                        $qOrders[$os[$i]['col']] = $os[$i]['dir'];
                    $sql = $_dialect->AppendOrder($sql,$qOrders);
                }
                $sql = $_dialect->LimitQuery($sql,$this->DisplayStart,$this->DisplayLength);

                $this->DataStatement->Append($sql);
                return $this->DataStatement->Execute();

            },function($fs,&$out) use(&$tablePrefix)
            {
                if ($this->FilteredCountStatement == null)
                    return -1;

                $_dialect = $this->FilteredCountStatement->GetDialect();

                $sql = '';
                if ($fs != null)
                {
                    if (count($fs) <= 0)
                        $fs = null;
                    else
                        $fs = array(
                            'condition' => 'AND',
                            'rules' => $fs
                        );
                    $converter = new QBConverter($fs,$_dialect);
                    $sql .= (" ".$converter->Convert($tablePrefix,true));
                }

                $this->FilteredCountStatement->Append($sql);
                $temp = $this->FilteredCountStatement->Execute()->FirstRow();
                if ($temp->Total == null)
                    throw new Exception('"Total" column in filtered count query is not exists.');
                return (int)$temp->Total;

            },function(&$out)
            {
                if ($this->TotalCountStatement == null)
                    return -1;

                $temp = $this->TotalCountStatement->Execute()->FirstRow();
                if ($temp->Total == null)
                    throw new Exception('"Total" column in total count query is not exists.');
                return (int)$temp->Total;
            });
        }
    }
}
?>