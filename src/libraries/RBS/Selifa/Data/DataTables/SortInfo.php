<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\DataTables;

/**
 * Class SortInfo
 * @package RBS\Selifa\Data\DataTables
 */
class SortInfo
{
    /**
     * @var int
     */
    public $ColumnIndex = -1;

    /**
     * @var string
     */
    public $ColumnName = '';

    /**
     * @var bool
     */
    public $IsSortable = false;

    /**
     * @var string
     */
    public $SortDirection = 'ASC';

    /**
     * @param array $data
     * @param string $keyName
     * @param mixed|null $default
     * @return mixed|null
     */
    protected function Extract($data, $keyName, $default = null)
    {
        if (isset($data[$keyName]))
            return $data[$keyName];
        return $default;
    }

    /**
     * @param array $postData
     * @param string $cIndex
     */
    public function __construct($postData, $cIndex)
    {
        $sortCol = (int)$this->Extract($postData, 'iSortCol_' . $cIndex, 0);
        $this->ColumnIndex = $sortCol;
        $this->ColumnName = $this->Extract($postData, 'mDataProp_' . $sortCol, '');
        $this->IsSortable = (boolean)$this->Extract($postData, 'bSortable_' . $sortCol, false);
        $this->SortDirection = strtoupper($this->Extract($postData, 'sSortDir_' . $cIndex, ''));
    }
}
?>