<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\DataTables;
use RBS\Selifa\Data\BaseDialect;
use RBS\Selifa\Data\SQL\ConditionMember;
use RBS\Selifa\Data\SQL\QueryConditions;
use RBS\Selifa\Data\SQL\TableObject;
use RBS\Selifa\Data\SQL\QueryObject;
use RBS\Selifa\Data\SQL;

/**
 * Class QBConverter
 * @package RBS\Selifa\Data\DataTables
 */
class QBConverter
{
    /**
     * @var null|array
     */
    private $_RawRules = null;

    /**
     * @var null|BaseDialect
     */
    private $_Dialect = null;

    /**
     * QBConverter constructor.
     * @param array $qbRules
     * @param BaseDialect $dialect
     */
    public function __construct($qbRules,$dialect)
    {
        $this->_RawRules = $qbRules;
        $this->_Dialect = $dialect;
    }

    /**
     * @param string $input
     * @return string
     */
    private function __convertOperator($input)
    {
        switch ($input)
        {
            case 'equal': return SQL_OPERATOR_EQUAL;
            case 'not_equal': return SQL_OPERATOR_NOT_EQUAL;
            case 'in': return SQL_OPERATOR_IN;
            case 'not_in' : return '';
            case 'less': return SQL_OPERATOR_LESS_THAN;
            case 'less_or_equal': return SQL_OPERATOR_LESS_THAN_EQUAL;
            case 'greater': return SQL_OPERATOR_GREATER_THAN;
            case 'greater_or_equal': return SQL_OPERATOR_GREATER_THAN_EQUAL;
            case 'between': return SQL_OPERATOR_BETWEEN;
            case 'not_between': return SQL_OPERATOR_NOT_BETWEEN;
            case 'begins_with': return SQL_OPERATOR_BEGINS_WITH;
            case 'not_begins_with': return SQL_OPERATOR_NOT_BEGINS_WITH;
            case 'contains': return SQL_OPERATOR_LIKE;
            case 'not_contains': return SQL_OPERATOR_NOT_LIKE;
            case 'ends_with': return SQL_OPERATOR_ENDS_WITH;
            case 'not_ends_with': return SQL_OPERATOR_NOT_ENDS_WITH;
            case 'is_null': return SQL_OPERATOR_IS_NULL;
            case 'is_not_null': return SQL_OPERATOR_IS_NOT_NULL;
            case 'is_empty': return SQL_OPERATOR_IS_EMPTY;
            case 'is_not_empty': return SQL_OPERATOR_IS_NOT_EMPTY;
            default: return SQL_OPERATOR_EQUAL;
        }
    }

    /**
     * @param TableObject $dummy
     * @param QueryConditions $conds
     * @param array $items
     */
    private function __convertRules($dummy,$conds,$items)
    {
        foreach ($items as $item)
        {
            if (isset($item['condition']))
            {
                $newConds = SQL::Condition();
                $this->__convertRules($dummy,$newConds,$item['rules']);
                $newConds->SetComparison(strtoupper(trim($item['condition'])));
                $conds->AddMember($newConds);
            }
            else
            {
                $condition = new ConditionMember();
                $condition->AddActor($dummy->{$item['field']});
                $condition->AddActor($item['value']);

                $operator = strtolower(trim($item['operator']));
                $condition->SetOperator($this->__convertOperator($operator));
                $conds->AddMember($condition);
            }
        }
    }

    /**
     * @param string $tableAlias
     * @param bool $toString
     * @return QueryObject|string
     */
    public function Convert($tableAlias='',$toString=false)
    {
        $table = SQL::Table('Dummy',$tableAlias)->AddColumn('*');
        $conds = SQL::Condition();
        $this->__convertRules($table,$conds,array($this->_RawRules));

        $query = SQL::Query($table);
        $query->SetCondition($conds);
        if ($toString)
            return $query->toSQLString($this->_Dialect,false,true);
        else
            return $query;
    }
}
?>