<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;
use RBS\Selifa\Template\ITGXObjectRenderer;
use RBS\Selifa\Template\TGX;
use JsonSerializable;

/**
 * Class DataTable
 * @package RBS\Selifa\Data
 */
class DataTable implements ITGXObjectRenderer, JsonSerializable
{
    /**
     * @var null|BaseDatabaseDriver
     */
    private $_Driver = null;

    /**
     * @var null|IDataExtension
     */
    private $_Extension = null;

    /**
     * @var null|resource|mixed
     */
    private $_ResultInstance = null;

    /**
     * @var bool
     */
    private $_IsFree = false;

    /**
     * @var null|array
     */
    private $_MetaData = null;

    /**
     * @var int
     */
    private $_DefaultRowMode = DB_ROW_MODE_UPDATE;

    /**
     * @var bool
     */
    private $_UseNumberedRow = false;

    /**
     * @var string
     */
    private $_RowNumberColumnName = '';

    /**
     * @var int
     */
    private $_CurrentRowNo = 1;

    /**
     * @var array
     */
    private $_Transformations = array();

    /**
     * @param BaseDatabaseDriver $driverObject
     * @param resource|mixed $resultInstance
     * @param IDataExtension $iExtension
     */
    public function __construct($driverObject,$resultInstance,$iExtension=null)
    {
        $this->_Driver = $driverObject;
        $this->_ResultInstance = $resultInstance;
        $this->_Extension = $iExtension;
        $this->_MetaData = $driverObject->_RawGetResultMetadata($resultInstance,'');

        if ($this->_MetaData['TableName'] == '')
            $this->_DefaultRowMode = DB_ROW_MODE_READ_ONLY;
    }

    /**
     *
     */
    public function __destruct()
    {
        if (!$this->_IsFree)
            $this->_Driver->_RawFreeResult($this->_ResultInstance);
    }

    /**
     * @param bool $processRow
     * @return array
     * @throws DatabaseException
     */
    public function ToArray($processRow=true)
    {
        if ($this->_IsFree)
            $this->_Driver->_ThrowDBError('Cannot fetch data due to resultset has been released.');
        $result = array();

        if ($processRow)
        {
            $rowNo = 1;
            while ($row = $this->Fetch())
            {
                $resultRow = array();
                if ($this->_UseNumberedRow)
                    $resultRow[$this->_RowNumberColumnName] = $rowNo;
                foreach ($this->_MetaData['ColumnInfo'] as $key => $md)
                    $resultRow[$key] = $row->{$key};
                $result[] = $resultRow;
            }
        }
        else
        {
            $rowNo = 1;
            while ($row = $this->_Driver->_RawFetch($this->_ResultInstance))
            {
                if ($this->_UseNumberedRow)
                    $row[$this->_RowNumberColumnName] = $rowNo;
                $result[] = $row;
                $rowNo++;
            }
            $this->_Driver->_RawFreeResult($this->_ResultInstance);
            $this->_IsFree = true;
        }
        return $result;
    }

    /**
     * @return bool|DataRow
     * @throws DatabaseException
     */
    public function Fetch()
    {
        if ($this->_IsFree)
            $this->_Driver->_ThrowDBError('Cannot fetch data due to resultset has been released.');

        $fetchedRow = $this->_Driver->_RawFetch($this->_ResultInstance);
        if ($fetchedRow === null)
        {
            $this->_Driver->_RawFreeResult($this->_ResultInstance);
            $this->_IsFree = true;
            return false;
        }

        if ($this->_UseNumberedRow)
            $fetchedRow[$this->_RowNumberColumnName] = $this->_CurrentRowNo;

        $fRow = new DataRow($this->_Driver,$fetchedRow,$this->_MetaData,$this->_DefaultRowMode,$this->_Extension,$this);
        if ($this->_Extension != null)
            $this->_Extension->RowFetched($fRow);

        $this->_CurrentRowNo++;
        return $fRow;
    }

    /**
     * @param callable|null $rowCallback
     * @param bool $toDataRow
     * @throws DatabaseException
     */
    public function Each($rowCallback,$toDataRow=true)
    {
        if ($this->_IsFree)
            $this->_Driver->_ThrowDBError('Cannot fetch data due to resultset has been released.');

        if (!is_callable($rowCallback))
            $this->_Driver->_ThrowDBError('rowCallback is not a callable type or null.');

        $rowNo = 1;
        while ($dbRow = $this->_Driver->_RawFetch($this->_ResultInstance))
        {
            if ($this->_UseNumberedRow)
                $dbRow[$this->_RowNumberColumnName] = $rowNo;
            if ($toDataRow)
            {
                $dataRow = new DataRow($this->_Driver, $dbRow, $this->_MetaData,$this->_DefaultRowMode,$this->_Extension,$this);
                if ($this->_Extension != null)
                    $this->_Extension->RowFetched($dataRow);
                $rowCallback($dbRow, $dataRow);
            }
            else
                $rowCallback($dbRow, null);
            $rowNo++;
        }

        $this->_Driver->_RawFreeResult($this->_ResultInstance);
        $this->_IsFree = true;
    }

    /**
     * @return DataRow|bool
     * @throws DatabaseException
     */
    public function FirstRow()
    {
        if ($this->_IsFree)
            $this->_Driver->_ThrowDBError('Cannot fetch data due to resultset has been released.');

        $dbRow = $this->_Driver->_RawFetch($this->_ResultInstance);
        if ($dbRow == false)
            return null;

        $dataRow = new DataRow($this->_Driver,$dbRow,$this->_MetaData,$this->_DefaultRowMode,$this->_Extension,$this);
        $this->_Driver->_RawFreeResult($this->_ResultInstance);
        $this->_IsFree = true;

        if ($this->_Extension != null)
            $this->_Extension->RowFetched($dataRow);
        return $dataRow;
    }

    /**
     * @param string $columnName
     * @return DataTable
     */
    public function WithRowNumbers($columnName='RowNo')
    {
        $this->_UseNumberedRow = true;
        $this->_RowNumberColumnName = $columnName;
        $this->_MetaData['ColumnInfo'][$columnName] = array(
            'value' => null,
            'name' => $columnName,
            'type' => 'int',
            'key' => false,
            'auto' => false,
            'modified' => false,
            'forcenull' => false
        );
        return $this;
    }

    /**
     * @return DataTable
     */
    public function ResetNumbers()
    {
        $this->_CurrentRowNo = 1;
        return $this;
    }

    /**
     * @param array|string $sourceOrKey
     * @param IRowTransformation|null $tObject
     * @return DataTable
     */
    public function RegisterTransformation($sourceOrKey,$tObject=null)
    {
        if (is_array($sourceOrKey))
            $this->_Transformations = array_merge_recursive($this->_Transformations,$sourceOrKey);
        else if (($tObject != null) && ($tObject instanceof IRowTransformation))
            $this->_Transformations[$sourceOrKey] = $tObject;
        return $this;
    }

    /**
     * @param string $key
     * @return IRowTransformation|null
     */
    public function GetTransformation($key)
    {
        if (isset($this->_Transformations[$key]))
            return $this->_Transformations[$key];
        else
            return null;
    }

    /**
     * @return array|null
     */
    public function GetMetaData()
    {
        return $this->_MetaData;
    }

    #region ITGXObjectRenderer implementation
    /**
     * @param TGX $tgxObject
     * @return array
     * @throws DatabaseException
     */
    public function TGXRender($tgxObject)
    {
        //return $this->ToArray();
        $result = array();
        while ($row = $this->Fetch())
            $result[] = $row->ToArray();
        return $result;
    }
    #endregion

    #region JsonSerializable implementation
    /**
     *
     */
    public function jsonSerialize()
    {
        $result = array();
        while ($row = $this->Fetch())
            $result[] = $row->ToArray();
        return $result;
    }
    #endregion
}
?>