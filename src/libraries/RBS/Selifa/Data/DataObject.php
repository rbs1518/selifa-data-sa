<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;
use \SelifaException;

/**
 * Class DataObject
 * @package RBS\Selifa\Data
 */
class DataObject
{
    /**
     * @var null|BaseDatabaseDriver
     */
    private $_Driver = null;

    /**
     * @var null|IDataExtension
     */
    private $_Extension = null;

    /**
     * @var array
     */
    private $_RSCMDS = array('SELECT','SHOW','DESC','DESCRIBE');

    /**
     * @var bool
     */
    private $_IsResultSet = false;

    /**
     * @var bool
     */
    private $_IsTable = false;

    /**
     * @var bool
     */
    private $_IsProcedure = false;

    /**
     * @var string
     */
    private $_SQL = '';

    /**
     * @var string
     */
    private $_ObjectName = '';

    /**
     * @var array
     */
    private $_Columns = array();

    /**
     * @var null|array
     */
    private $_TableMetaData = null;

    /**
     * @var int
     */
    public $DataTableCount = 0;

    /**
     * @param BaseDatabaseDriver $driverObject
     * @param string $sqlCommand
     * @param boolean $isTable
     * @param boolean $isProcedure
     * @param string $tableName
     * @param IDataExtension $iExtension
     * @throws SelifaException
     */
    public function __construct($driverObject,$sqlCommand,$isTable,$isProcedure,$tableName='',$iExtension=null)
    {
        $this->_Driver = $driverObject;
        $this->_IsTable = $isTable;
        $this->_IsProcedure = $isProcedure;
        $this->_Extension = $iExtension;

        if (!$this->_IsTable)
        {
            if ($sqlCommand == '')
                $this->_Driver->_ThrowDBError('SQL command is empty.');
            $iPos = strpos($sqlCommand,' ',0);
            $sCheck = ($iPos < 0?strtoupper($sqlCommand):strtoupper(substr($sqlCommand,0,$iPos)));
            $this->_IsResultSet = in_array($sCheck,$this->_RSCMDS);

            if ($isProcedure)
                $this->_ObjectName = ($driverObject->EnclosureStart.$sqlCommand.$driverObject->EnclosureEnd);
            else
                $this->_SQL = $sqlCommand;
        }
        else
        {
            $this->_IsResultSet = true;
            $this->_ObjectName = $tableName;
            $this->_TableMetaData = $driverObject->_RawGetResultMetadata(null,$tableName);
        }
    }

    /**
     *
     */
    private function __TerminateMultiQuery()
    {
        if ($this->_Driver->SupportMultiQuery)
        {
            while ($this->_Driver->_RawHasMoreResult())
                $this->_Driver->_RawNextResult();
        }
    }

    /**
     * @param mixed $qResult
     * @param string $errorMessage
     * @return DataTable[]|null|DataTable
     * @throws SelifaException
     */
    private function __GetDataTables($qResult,$errorMessage)
    {
        if (!$this->_Driver->SupportMultiQuery)
        {
            $this->DataTableCount = 1;
            return new DataTable($this->_Driver,$qResult,$this->_Extension);
        }
        else
        {
            $dtCount = 0;
            $dtArray = array();
            while (true)
            {
                $aResult = $this->_Driver->_RawGetResult();
                if ($aResult === false)
                {
                    if ($dtCount <= 0)
                        $this->_Driver->_ThrowDBError($errorMessage);
                    else
                    {
                        $this->_Driver->_RawNextResult();
                        break;
                    }
                }

                $dtArray[] = new DataTable($this->_Driver,$aResult,$this->_Extension);
                $dtCount++;

                if (!$this->_Driver->_RawHasMoreResult())
                {
                    $this->_Driver->_RawNextResult();
                    break;
                }
                else
                    $this->_Driver->_RawNextResult();
            }

            $this->DataTableCount = $dtCount;
            if ($dtCount <= 0)
                return null;
            else if ($dtCount == 1)
                return $dtArray[0];
            else
                return $dtArray;
        }
    }

    /**
     * @param mixed $qResult
     * @return mixed|array|null
     */
    private function __GetFirstFetchedData($qResult)
    {
        $fetchedResult = null;
        if ($this->_Driver->SupportMultiQuery)
        {
            $storedResult = $this->_Driver->_RawGetResult();
            if ($storedResult !== false)
            {
                $fetchedResult = $this->_Driver->_RawFetch($storedResult);
                $this->_Driver->_RawFreeResult($storedResult);
            }
            $this->__TerminateMultiQuery();
        }
        else
        {
            $fetchedResult = $this->_Driver->_RawFetch($qResult);
            if ($fetchedResult !== false)
            {
                $this->_Driver->_RawFreeResult($qResult);
                $this->_Driver->_RawNextResult();
            }
            else
                $fetchedResult = null;
        }
        return $fetchedResult;
    }

    /**
     * @return string
     */
    private function __CreateDefaultTableQuery()
    {
        $query = "select ";
        if (count($this->_Columns) > 0)
        {
            $glue = ($this->_Driver->EnclosureEnd.', '.$this->_Driver->EnclosureStart);
            $query .= ($this->_Driver->EnclosureStart.implode($glue,$this->_Columns).$this->_Driver->EnclosureStart);
        }
        else
            $query .= "*";
        $query .= (" from ".$this->_Driver->EnclosureStart.$this->_ObjectName.$this->_Driver->EnclosureEnd);
        return $query;
    }

    /**
     * @param array $aData
     * @return string
     */
    private function __ArrayToWhereQueryOnly($aData)
    {
        if (count($aData) > 0)
        {
            $temps = array();
            foreach ($aData as $column => $value)
            {
                if (is_string($value))
                    $aValue = ("'".$this->_Driver->EscapeString($value)."'");
                else
                    $aValue = $this->_Driver->EscapeString($value);
                $temps[] = ($this->_Driver->EnclosureStart . $column . $this->_Driver->EnclosureEnd . ' = ' . $aValue);
            }
            return ('where ('.implode(') AND (',$temps).')');
        }
        return '';
    }

    /**
     * @param array $source
     * @return string
     */
    protected function ArrayToParameterString($source)
    {
        $s = '';
        foreach ($source as $item)
        {
            if (is_string($item))
                $s .= (",'".$this->_Driver->EscapeString($item)."'");
            else
                $s .= (','.$this->_Driver->EscapeString($item));
        }
        $s[0] = ' ';
        return $s;
    }

    /**
     * @return DataObject
     */
    public function Select()
    {
        $args = func_get_args();
        if (count($args) > 0)
        {
            if (is_array($args[0]))
                $this->_Columns = $args[0];
            else
                $this->_Columns = $args;
        }
        return $this;
    }

    /**
     * @return null|DataTable|DataTable[]
     * @throws SelifaException
     */
    public function GetData()
    {
        if (!$this->_IsResultSet && !$this->_IsProcedure)
            $this->_Driver->_ThrowDBError('Mismatch SQL command for GetData.');

        $parameters = func_get_args();
        if ($this->_IsProcedure)
        {
            $this->_SQL = $this->_Driver->_RawStoredProcedureSQL($this->_ObjectName,$parameters);
            $qResult = $this->_Driver->_RawQuery($this->_SQL);
            //$qResult = $this->_Driver->_RawSingleQuery($this->_SQL);
            if ($qResult === false)
                $this->_Driver->_ThrowDBError('Cannot execute stored procedure: '.$this->_ObjectName);
            return $this->__GetDataTables($qResult,'Cannot retrieve result from stored procedure.');
        }
        else if ($this->_IsTable)
        {
            $this->_SQL = $this->__CreateDefaultTableQuery();
            $qResult = $this->_Driver->_RawQuery($this->_SQL);
            if ($qResult === false)
                $this->_Driver->_ThrowDBError('Query execution has failed.');
            return $this->__GetDataTables($qResult,'Cannot retrieve result from specified table.');
        }
        else
        {
            $this->_SQL = $this->_Driver->_ParseQuery($this->_SQL,$parameters);
            $qResult = $this->_Driver->_RawQuery($this->_SQL);
            if ($qResult === false)
                $this->_Driver->_ThrowDBError('Query execution has failed.');
            return $this->__GetDataTables($qResult,'Cannot retrieve result from query.');
        }
    }

    /**
     * @return array|bool|mixed|null|resource
     * @throws SelifaException
     */
    public function Run()
    {
        if ($this->_IsResultSet && !$this->_IsProcedure)
            $this->_Driver->_ThrowDBError('Mismatch SQL command for Run.');

        $parameters = func_get_args();
        if ($this->_IsProcedure)
        {
            $this->_SQL = $this->_Driver->_RawStoredProcedureSQL($this->_ObjectName,$parameters);
            $qResult = $this->_Driver->_RawQuery($this->_SQL);
            if ($qResult === false)
                $this->_Driver->_ThrowDBError('Cannot execute stored procedure: '.$this->_ObjectName);
            return $this->__GetFirstFetchedData($qResult);
        }
        else
        {
            $this->_SQL = $this->_Driver->_ParseQuery($this->_SQL,$parameters);
            $qResult = $this->_Driver->_RawSingleQuery($this->_SQL);
            if ($qResult === false)
                $this->_Driver->_ThrowDBError('Query execution has failed.');

            if (is_bool($qResult))
                return $qResult;
            else if (is_integer($qResult))
                return ($qResult > 0);
            else
                $this->_Driver->_ThrowDBError('Unknown query execution result. It is not boolean nor integer. ('.$qResult.')');
        }

        return null;
    }

    /**
     * @param array|string|int $byIdOrFields
     * @return null|DataRow
     * @throws SelifaException
     */
    public function GetRow($byIdOrFields)
    {
        if (!$this->_IsTable)
            $this->_Driver->_ThrowDBError('Invalid DataObject type for GetRow.');

        $filters = array();
        if (is_array($byIdOrFields))
            $filters = $byIdOrFields;
        else
        {
            if (!$this->_TableMetaData['IsPrimaryKeyAvailable'])
                $this->_Driver->_ThrowDBError('Primary key is not exists for table '.$this->_ObjectName.'.');
            $firstPKName = $this->_TableMetaData['PrimaryKeys'][0];
            $filters[$firstPKName] = $byIdOrFields;
        }

        if ($this->_Extension != null)
            $this->_Extension->FilterRow($this,$filters);
        $sql = ($this->__CreateDefaultTableQuery() . ' ' . $this->__ArrayToWhereQueryOnly($filters));

        if ($this->_Driver->SupportMultiQuery)
        {
            $this->_Driver->_RawQuery($sql);
            $qResult = $this->_Driver->_RawGetResult();
            $this->_Driver->_RawNextResult();
        }
        else
            $qResult = $this->_Driver->_RawQuery($sql);

        $fetchedRow = $this->_Driver->_RawFetch($qResult);
        if ($fetchedRow === null)
            return null;
        $this->_Driver->_RawFreeResult($qResult);

        if ($this->_TableMetaData['TableName'] == '')
            $fRow = new DataRow($this->_Driver,$fetchedRow,$this->_TableMetaData,DB_ROW_MODE_READ_ONLY,$this->_Extension);
        else
            $fRow = new DataRow($this->_Driver,$fetchedRow,$this->_TableMetaData,DB_ROW_MODE_UPDATE,$this->_Extension);
        if ($this->_Extension != null)
            $this->_Extension->RowFetched($fRow);
        return $fRow;
    }

    /**
     * @param null|array $aData
     * @return DataRow
     */
    public function NewRow($aData=null)
    {
        if ($aData === null)
            $newRow = new DataRow($this->_Driver,array(),$this->_TableMetaData,DB_ROW_MODE_INSERT,$this->_Extension);
        else
            $newRow = new DataRow($this->_Driver,$aData,$this->_TableMetaData,DB_ROW_MODE_INSERT,$this->_Extension);
        if ($this->_Extension != null)
            $this->_Extension->NewRowCreated($newRow);
        return $newRow;
    }
}
?>