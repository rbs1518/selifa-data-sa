<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;

/**
 * Interface IDataExtension
 * @package RBS\Selifa\Data
 */
interface IDataExtension
{
    /**
     * @return array|null
     */
    public function GetInitialData();

    /**
     * @param DataRow $newRow
     * @return mixed|null
     */
    public function NewRowCreated($newRow);

    /**
     * @param DataObject $dataObject
     * @param array $filters
     * @return mixed
     */
    public function FilterRow($dataObject,&$filters);

    /**
     * @param DataRow $dbRow
     * @return mixed
     */
    public function RowFetched($dbRow);

    /**
     * @param DataRow $dbRow
     * @param int $rowMode
     * @param bool $cancelSave
     */
    public function SaveRow($dbRow,$rowMode,&$cancelSave);

    /**
     * @param DataRow $dbRow
     * @param int $rowMode
     */
    public function RowSaved($dbRow,$rowMode);

    /**
     * @param DataRow $dbRow
     * @param bool $cancelDelete
     * @param bool $doUpdateInstead
     */
    public function DeleteRow($dbRow,&$cancelDelete,&$doUpdateInstead);

    /**
     * @param DataRow $dbRow
     * @return mixed
     */
    public function RowDeleted($dbRow);
}
?>