<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;

/**
 * Basic SQL dialect class. Inherit within each database driver.
 *
 * @package RBS\Selifa\Data
 */
abstract class BaseDialect
{
    /**
     * @var string
     */
    public $S = '';

    /**
     * @var string
     */
    public $E = '';

    /**
     * @var string
     */
    public $DefaultJoin = 'join';

    /**
     * @var string
     */
    public $InnerJoin = 'inner join';

    /**
     * @var string
     */
    public $OuterJoin = 'outer join';

    /**
     * @var string
     */
    public $LeftJoin = 'left join';

    /**
     * @var string
     */
    public $RightJoin = 'right join';

    /**
     * @var string
     */
    public $AND = 'and';

    /**
     * @var string
     */
    public $OR = 'or';

    /**
     * @var string
     */
    public $Equal = '%s = %s';

    /**
     * @var string
     */
    public $NotEqual = '%s <> %s';

    /**
     * @var string
     */
    public $Like = '%s like %s';

    /**
     * @var string
     */
    public $NotLike = '%s not like %s';

    /**
     * @var string
     */
    public $StrictLike = '%s like %s';

    /**
     * @var string
     */
    public $StrictNotLike = '%s not like %s';

    /**
     * @var string
     */
    public $BeginsWith = '%s like %s';

    /**
     * @var string
     */
    public $EndsWith = '%s like %s';

    /**
     * @var string
     */
    public $NotBeginsWith = '%s not like %s';

    /**
     * @var string
     */
    public $NotEndsWith = '%s not like %s';

    /**
     * @var string
     */
    public $GreaterThan = '%s > %s';

    /**
     * @var string
     */
    public $LessThan = '%s < %s';

    /**
     * @var string
     */
    public $GreaterThanEqual = '%s >= %s';

    /**
     * @var string
     */
    public $LessThanEqual = '%s <= %s';

    /**
     * @var string
     */
    public $IsNull = '%s is null';

    /**
     * @var string
     */
    public $IsNotNull = '%s is not null';

    /**
     * @var string
     */
    public $Between = '%s between %s and %s';

    /**
     * @var string
     */
    public $NotBetween = '%s not between %s and %s';

    /**
     * @var string
     */
    public $In = '%s in (%s)';

    /**
     * @var string
     */
    public $NotIn = '%s not in (%s)';

    /**
     * @var string
     */
    public $IsEmpty = "%s = ''";

    /**
     * @var string
     */
    public $IsNotEmpty = "%s <> ''";

    /**
     * @var string
     */
    public $Ascending = 'asc';

    /**
     * @var string
     */
    public $Descending = 'desc';

    /**
     * @var string
     */
    public $As = 'as';

    /**
     * @var bool
     */
    public $UseLowerCaseTableName = false;

    /**
     * @param string $sql
     * @param int $offset
     * @param int $count
     * @return mixed
     */
    public function LimitQuery($sql,$offset,$count) { return $sql; }

    /**
     * @param string $sql
     * @param array $columns
     * @return mixed
     */
    public function AppendOrder($sql,$columns)
    {
        $temp = array();
        foreach ($columns as $key => $order)
            $temp[] = ($this->S.$key.$this->E.' '.$order);
        return ($sql.' order by '.implode(', ',$temp));
    }

    /**
     * @param string $sql
     * @param string $alias
     * @return mixed
     */
    public function WrapInSelect($sql,$alias)
    {
        return ('select '.$alias.'.* from ('.$sql.') as '.$alias);
    }
}
?>