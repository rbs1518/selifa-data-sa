<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data;
use RBS\Selifa\Data\SQL\TableObject;
use RBS\Selifa\Data\SQL\QueryObject;
use RBS\Selifa\Data\SQL\QueryConditions;
use RBS\Selifa\Data\SQL\RowOrder;

#region Constants
define('SQL_JOIN_DEFAULT', 'DefaultJoin');
define('SQL_JOIN_INNER', 'InnerJoin');
define('SQL_JOIN_OUTER', 'OuterJoin');
define('SQL_JOIN_LEFT', 'LeftJoin');
define('SQL_JOIN_RIGHT', 'RightJoin');

define('SQL_COMPARISON_AND', 'AND');
define('SQL_COMPARISON_OR', 'OR');

define('SQL_OPERATOR_EQUAL', 'Equal');
define('SQL_OPERATOR_NOT_EQUAL', 'NotEqual');
define('SQL_OPERATOR_LIKE', 'Like');
define('SQL_OPERATOR_NOT_LIKE','NotLike');
define('SQL_OPERATOR_STRICT_LIKE','StrictLike');
define('SQL_OPERATOR_STRICT_NOT_LIKE','StrictNotLike');
define('SQL_OPERATOR_GREATER_THAN', 'GreaterThan');
define('SQL_OPERATOR_GREATER_THAN_EQUAL', 'GreaterThanEqual');
define('SQL_OPERATOR_LESS_THAN', 'LessThan');
define('SQL_OPERATOR_LESS_THAN_EQUAL', 'LessThanEqual');
define('SQL_OPERATOR_IS_NULL', 'IsNull');
define('SQL_OPERATOR_IS_NOT_NULL', 'IsNotNull');
define('SQL_OPERATOR_BETWEEN', 'Between');
define('SQL_OPERATOR_NOT_BETWEEN','NotBetween');
define('SQL_OPERATOR_IN','In');
define('SQL_OPERATOR_NOT_INT','NotIn');
define('SQL_OPERATOR_BEGINS_WITH','BeginsWith');
define('SQL_OPERATOR_ENDS_WITH','EndsWith');
define('SQL_OPERATOR_NOT_BEGINS_WITH','NotBeginsWith');
define('SQL_OPERATOR_NOT_ENDS_WITH','NotEndsWith');
define('SQL_OPERATOR_IS_EMPTY','IsEmpty');
define('SQL_OPERATOR_IS_NOT_EMPTY','IsNotEmpty');

define('SQL_ORDER_ASCENDING', 'Ascending');
define('SQL_ORDER_DESCENDING', 'Descending');

define('SQL_COLUMN_OPERATOR_ADDITION', '+');
define('SQL_COLUMN_OPERATOR_SUBTRACTION', '-');
define('SQL_COLUMN_OPERATOR_MULTIPLICATION', '*');
define('SQL_COLUMN_OPERATOR_DIVISION', '/');
#endregion

/**
 * SQL Builder bootstrap class.
 *
 * @package RBS\Selifa\Data
 */
class SQL
{
    /**
     * @param string $name
     * @param string $alias
     * @return TableObject
     */
    public static function Table($name, $alias = '')
    {
        return new TableObject($name, $alias);
    }

    /**
     * @param TableObject $tableObject
     * @param string $alias
     * @return QueryObject
     */
    public static function Query($tableObject, $alias = '')
    {
        return new QueryObject($tableObject, $alias);
    }

    /**
     * @return QueryConditions
     */
    public static function Condition()
    {
        $args = func_get_args();
        return new QueryConditions($args);
    }

    /**
     * @return RowOrder
     */
    public static function Order()
    {
        $args = func_get_args();
        return new RowOrder($args);
    }
}
?>