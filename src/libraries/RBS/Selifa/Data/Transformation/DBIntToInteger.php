<?php
namespace RBS\Selifa\Data\Transformation;
use RBS\Selifa\Data\IRowTransformation;

/**
 * Class DBIntToInteger
 * @package MCU\Billing\Transformation
 */
class DBIntToInteger implements IRowTransformation
{
    /**
     * @param mixed $value
     * @return int
     */
    public function ConvertTo($value)
    {
        return (int)$value;
    }

    /**
     * @param int $value
     * @return string
     */
    public function ConvertFrom($value)
    {
        return (string)$value;
    }
}
?>