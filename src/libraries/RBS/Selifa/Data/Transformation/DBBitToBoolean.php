<?php
namespace RBS\Selifa\Data\Transformation;
use RBS\Selifa\Data\IRowTransformation;

/**
 * Class DBBitToBoolean
 * @package MCU\Billing\Transformation
 */
class DBBitToBoolean implements IRowTransformation
{
    /**
     * @param mixed $value
     * @return bool
     */
    public function ConvertTo($value)
    {
        $iValue = (int)$value;
        return ($iValue == 1);
    }

    /**
     * @param mixed $value
     * @return int
     */
    public function ConvertFrom($value)
    {
        return ($value?1:0);
    }
}
?>