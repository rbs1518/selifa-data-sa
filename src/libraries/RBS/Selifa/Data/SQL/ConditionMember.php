<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class ConditionMember
 * @package RBS\Selifa\Data\SQL
 */
class ConditionMember extends BaseSQLObject
{
    /**
     * @var string
     */
    protected $Operator = SQL_OPERATOR_EQUAL;

    /**
     * @var BaseSQLObject[]
     */
    protected $Actor = array();

    /**
     * @var bool
     */
    public $StrictLikeOperator = false;

    /**
     *
     */
    public function __construct()
    {
        $args = func_get_args();
        if (func_num_args() > 0)
        {
            if (is_array($args[0]))
                $this->Actor = $args[0];
            else
                $this->Actor = $args;
        }
    }

    /**
     * @return ConditionMember
     */
    public static function Create()
    {
        $args = func_get_args();
        return new ConditionMember($args);
    }

    /**
     * @param BaseSQLObject|string|integer $qObject
     * @return $this
     */
    public function AddActor($qObject)
    {
        $this->Actor[] = $qObject;
        return $this;
    }

    /**
     * @param string $operator
     * @return $this
     */
    public function SetOperator($operator)
    {
        $this->Operator = $operator;
        return $this;
    }

    /**
     * @return $this
     */
    public function SetStrictLIKE()
    {
        $this->StrictLikeOperator = true;
        return $this;
    }

    /**
     * @param BaseDialect $dialect
     * @param bool $isAliased
     * @return string
     */
    public function toSQLString(BaseDialect $dialect, $isAliased)
    {
        $stringifiedActor = array();
        foreach ($this->Actor as $actor)
        {
            if ($actor instanceof BaseSQLObject)
                $stringifiedActor[] = $actor->toSQLString($dialect,false);
            else
            {
                if (is_string($actor))
                {
                    if (($this->Operator == SQL_OPERATOR_LIKE) || ($this->Operator == SQL_OPERATOR_NOT_LIKE))
                        $stringifiedActor[] = ("'%" . $actor . "%'");
                    else if (($this->Operator == SQL_OPERATOR_BEGINS_WITH) || ($this->Operator == SQL_OPERATOR_NOT_BEGINS_WITH))
                        $stringifiedActor[] = ("'" . $actor . "%'");
                    else if (($this->Operator == SQL_OPERATOR_ENDS_WITH) || ($this->Operator == SQL_OPERATOR_NOT_ENDS_WITH))
                        $stringifiedActor[] = ("'%" . $actor . "'");
                    else if (($this->Operator == SQL_OPERATOR_STRICT_LIKE) || ($this->Operator == SQL_OPERATOR_STRICT_NOT_LIKE))
                        $stringifiedActor[] = ("'" . $actor . "'");
                    else
                        $stringifiedActor[] = ("'" . $actor . "'");
                }
                else if (is_array($actor))
                {
                    $components = array();
                    foreach ($actor as $item)
                    {
                        if (is_string($item))
                            $components[] = ("'".$item."'");
                        else
                            $components[] = $item;
                    }
                    $stringifiedActor[] = implode(',',$components);
                }
                else
                    $stringifiedActor[] = (string)$actor;
            }
        }
        return ('(' . vsprintf($dialect->{$this->Operator}, $stringifiedActor) . ')');
    }
}
?>