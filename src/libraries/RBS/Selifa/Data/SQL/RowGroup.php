<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class RowGroup
 * @package RBS\Selifa\Data\SQL
 */
class RowGroup extends BaseSQLObject
{
    /**
     * @var ColumnObject[]
     */
    protected $Members = array();

    /**
     *
     */
    public function __construct()
    {
        $this->Members = func_get_args();
    }

    /**
     * @param ColumnObject $cObject
     */
    public function AddMember($cObject)
    {
        $this->Members[] = $cObject;
    }

    /**
     * @param BaseDialect $dialect
     * @param bool $isAliased
     * @return string
     */
    public function toSQLString(BaseDialect $dialect, $isAliased)
    {
        $stringifiedMembers = '';
        foreach ($this->Members as $member)
        {
            if ($member instanceof ColumnObject)
            {
                if ($member->Owner->Alias != '')
                    $stringifiedMembers[] = ($dialect->S . $member->Owner->Alias . $dialect->E.'.'.$dialect->S . $member->Name . $dialect->E);
                else
                    $stringifiedMembers[] = ($dialect->S . $member->Owner->Name . $dialect->E.'.'.$dialect->S . $member->Name . $dialect->E);
            }
        }
        return implode(', ', $stringifiedMembers);
    }
}
?>