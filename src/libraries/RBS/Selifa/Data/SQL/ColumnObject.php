<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;

use RBS\Selifa\Data\BaseDialect;

/**
 * Class ColumnObject
 * @package RBS\Selifa\Data\SQL
 */
class ColumnObject extends BaseSQLObject
{
    /**
     * @var string
     */
    public $Name = '';

    /**
     * @var BaseTableObject|QueryObject|TableObject|null
     */
    public $Owner = null;

    /**
     * @var bool
     */
    public $IgnoreOnSelect = false;

    /**
     * @param string $name
     * @param string $alias
     * @param BaseTableObject $owner
     */
    public function __construct($name, $alias, $owner)
    {
        $this->Name = $name;
        $this->Alias = $alias;
        $this->Owner = $owner;
    }

    /**
     * @param BaseDialect $dialect
     * @param bool $isAliased
     * @return string
     */
    public function toSQLString(BaseDialect $dialect, $isAliased)
    {
        $s = '';
        if ($this->Owner != null)
        {
            if ($this->Owner instanceof QueryObject)
                $s .= ($dialect->S.$this->Owner->Alias.$dialect->E);
            else if ($this->Owner instanceof TableObject)
                $s .= ($dialect->S.$this->Owner->GetAliasedName().$dialect->E);
        }
        $s .= ('.'.$dialect->S.$this->Name.$dialect->E);
        if ($isAliased)
        {
            if ($this->Alias != '')
                $s .= (' '.$dialect->As.' '.$dialect->S.$this->Alias.$dialect->E);
        }
        return $s;
    }
}
?>