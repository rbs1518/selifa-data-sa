<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;

abstract class BaseTableObject extends BaseSQLObject
{
    /**
     * @var TableObject|null
     */
    public $JoinWith = null;

    /**
     * @var string
     */
    public $JoinType = SQL_JOIN_DEFAULT;

    /**
     * @var QueryConditions
     */
    public $JoinCondition = null;

    /**
     * @var ColumnObject[]
     */
    public $Columns = array();

    /**
     * @var BaseSQLObject[]
     */
    public $ColumnFunctions = array();

    /**
     * @param BaseSQLObject|string $nameOrObject
     * @param string $alias
     * @param bool $ignored
     * @return BaseTableObject|TableObject|QueryObject
     */
    public function AddColumn($nameOrObject, $alias = '', $ignored = false)
    {
        if (is_string($nameOrObject))
        {
            $columnName = ($alias != '' ? $alias : $nameOrObject);
            $obj = new ColumnObject($nameOrObject, $alias, $this);
            $obj->IgnoreOnSelect = $ignored;

            $this->Columns[$columnName] = $obj;
        }
        else
        {
            if ($nameOrObject instanceof BaseSQLObject)
            {
                $nameOrObject->Alias = $alias;
                $this->ColumnFunctions[$alias] = $nameOrObject;
            }
        }

        return $this;
    }

    /**
     * @return BaseTableObject|TableObject|QueryObject
     */
    public function AddColumns()
    {
        $args = func_get_args();
        foreach ($args as $arg)
        {
            if (is_array($arg))
            {
                $aCount = count($arg);
                if ($aCount >= 3)
                {
                    $this->AddColumn($arg[0], $arg[1], $arg[2]);
                }
                else
                {
                    if ($aCount >= 2)
                    {
                        $this->AddColumn($arg[0], $arg[1]);
                    }
                    else
                    {
                        $this->AddColumn($arg[0]);
                    }
                }

            }
            else
            {
                $this->AddColumn($arg);
            }
        }

        return $this;
    }

    /**
     * @param BaseSQLObject $object
     * @param string $alias
     * @return TableObject
     */
    public function AddFunction($object, $alias = '')
    {
        $object->Alias = $alias;
        $this->ColumnFunctions[$alias] = $object;

        return $this;
    }

    /**
     * @param string $name
     * @return BaseSQLObject|ColumnObject|null
     */
    public function __get($name)
    {
        if (isset($this->Columns[$name]))
        {
            return $this->Columns[$name];
        }
        else
        {
            if (isset($this->ColumnFunctions[$name]))
            {
                return $this->ColumnFunctions[$name];
            }
            else
            {
                $cObject = new ColumnObject($name, '', $this);
                $cObject->IgnoreOnSelect = true;

                $this->Columns[$name] = $cObject;
                return $cObject;
            }
        }
    }

    /**
     * @param string $name
     * @return BaseSQLObject|ColumnObject|null
     */
    public function GetRegisteredColumn($name)
    {
        if (isset($this->Columns[$name]))
        {
            return $this->Columns[$name];
        }
        else
        {
            if (isset($this->ColumnFunctions[$name]))
            {
                return $this->ColumnFunctions[$name];
            }
        }

        return null;
    }
}
?>