<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class QueryObject
 * @package RBS\Selifa\Data\SQL
 */
class QueryObject extends BaseTableObject
{
    /**
     * @var BaseTableObject[]
     */
    protected $Tables = array();

    /**
     * @var QueryObject[]
     */
    protected $SubQueries = array();

    /**
     * @var string
     */
    protected $LastTableInjectedId = '';

    /**
     * @var QueryConditions|null
     */
    protected $Condition = null;

    /**
     * @var QueryConditions|null
     */
    protected $GroupCondition = null;

    /**
     * @var RowGroup|null
     */
    protected $RowGroup = null;

    /**
     * @var RowOrder|null
     */
    protected $Order = null;

    /**
     * @var RowLimit|null
     */
    protected $Limit = null;

    /**
     * @var bool
     */
    public $EnableTotalCountMode = false;

    /**
     * @param TableObject $tableObject
     * @param string $alias
     */
    public function __construct($tableObject, $alias = '')
    {
        $tableName = $tableObject->GetAliasedName();
        $this->Tables[$tableName] = $tableObject;

        $this->LastTableInjectedId = $tableName;
        $this->Alias = $alias;
    }

    /**
     * @param TableObject $tableObject
     * @param string $alias
     * @return QueryObject
     */
    public static function Create($tableObject, $alias = '')
    {
        $q = new QueryObject($tableObject, $alias);
        return $q;
    }

    /**
     * @param TableObject $tableObject
     * @param QueryConditions $joinCondition
     * @param string $joinType
     * @return QueryObject
     */
    public function Join($tableObject, $joinCondition, $joinType = SQL_JOIN_DEFAULT)
    {
        $previousTable = $this->Tables[$this->LastTableInjectedId];
        $previousTable->JoinType = $joinType;
        $previousTable->JoinWith = $tableObject;

        $tableObject->JoinCondition = $joinCondition;

        $tableName = ($tableObject->Alias != '' ? $tableObject->Alias : $tableObject->Name);
        $this->Tables[$tableName] = $tableObject;

        $this->LastTableInjectedId = $tableName;

        return $this;
    }

    /**
     * @param QueryObject $subQuery
     * @param string $alias
     * @return QueryObject
     */
    public function AddSubQuery($subQuery, $alias = '')
    {
        $subQuery->Alias = $alias;
        $this->SubQueries[] = $subQuery;

        return $this;
    }

    /**
     * @param QueryConditions $cObject
     * @return QueryObject
     */
    public function SetCondition($cObject)
    {
        $this->Condition = $cObject;
        return $this;
    }

    /**
     * @return QueryConditions|null
     */
    public function GetCondition()
    {
        return $this->Condition;
    }

    /**
     * @param QueryConditions $cObject
     * @return QueryObject
     */
    public function SetGroupCondition($cObject)
    {
        $this->GroupCondition = $cObject;
        return $this;
    }

    /**
     * @return QueryConditions|null
     */
    public function GetGroupCondition()
    {
        return $this->GroupCondition;
    }

    /**
     * @param RowGroup $rgObject
     * @return QueryObject
     */
    public function SetRowGroup($rgObject)
    {
        $this->RowGroup = $rgObject;
        return $this;
    }

    /**
     * @param RowOrder $oObject
     * @return QueryObject
     */
    public function SetOrder($oObject)
    {
        $this->Order = $oObject;
        return $this;
    }

    /**
     * @param RowLimit $limitObject
     * @return QueryObject
     */
    public function SetLimit($limitObject)
    {
        $this->Limit = $limitObject;
        return $this;
    }

    /**
     * @param $name
     * @return null|TableObject
     */
    public function __get($name)
    {
        if (isset($this->Tables[$name]))
        {
            return $this->Tables[$name];
        }
        else
        {
            return null;
        }
    }

    /**
     * @param string $acName
     * @return ColumnObject|null
     */
    public function FindColumn($acName)
    {
        $resultColumn = null;
        foreach ($this->Tables as $to)
        {
            if ($to instanceof TableObject)
            {
                $resultColumn = $to->GetRegisteredColumn($acName);
                if ($resultColumn != null)
                {
                    if ($resultColumn instanceof ColumnObject)
                    {
                        return $resultColumn;
                    }
                }
            }
        }

        return $resultColumn;
    }

    /**
     * @param BaseDialect $dialect
     * @param bool $isAliased
     * @param bool $skipSelect
     * @return string
     */
    public function toSQLString(BaseDialect $dialect, $isAliased, $skipSelect=false)
    {
        $s = '';
        if (!$skipSelect)
        {
            $a = ' from ';
            foreach ($this->Tables as $atName => $tableObject)
            {
                if (!$this->EnableTotalCountMode)
                {
                    foreach ($tableObject->Columns as $cN => $cO)
                        if (!$cO->IgnoreOnSelect)
                            $s .= (', ' . $cO->toSQLString($dialect,true));
                    foreach ($tableObject->ColumnFunctions as $cfn => $cfo)
                        $s .= (', ' . $cfo->toSQLString($dialect,true));
                }

                if ($tableObject instanceof TableObject)
                {
                    if ($dialect->UseLowerCaseTableName)
                        $a .= ($dialect->S.strtolower($tableObject->getTableName()).$dialect->E);
                    else
                        $a .= ($dialect->S.$tableObject->getTableName().$dialect->E);
                    $a .= ($tableObject->Alias != ''?' '.$dialect->As.' '.$dialect->S.$tableObject->Alias.$dialect->E:'');
                }
                else
                {
                    if ($tableObject instanceof QueryObject)
                        $a .= ('(' . $tableObject->toSQLString($dialect,false) . ') '.$dialect->As.' '.$dialect->S . $tableObject->Alias . $dialect->E);
                }

                if ($tableObject->JoinCondition instanceof QueryConditions)
                    $a .= ' on ' . $tableObject->JoinCondition->toSQLString($dialect,false);

                if ($tableObject->JoinWith != null)
                    $a .= (' ' . $dialect->{$tableObject->JoinType} . ' ');
                else
                    $a .= ' ';
            }

            foreach ($this->SubQueries as $sq)
            {
                if ($sq instanceof QueryObject)
                {
                    $s .= (', (' . $sq->toSQLString($dialect,false) . ')');
                    if ($sq->Alias != '')
                        $s .= (' '.$dialect->As.' '.$dialect->S . $sq->Alias . $dialect->E);
                }
            }

            if (!$this->EnableTotalCountMode)
            {
                //$s[0] = 't';
                $s[0] = ' ';
                $s = 'select' . $s . $a;
            }
            else
                $s = ("select count(*) as ".$dialect->S."TotalCount".$dialect->E.$a);
        }

        if ($this->Condition != null)
            if ($this->Condition instanceof QueryConditions)
                $s .= ('where ' . $this->Condition->toSQLString($dialect,false));

        if ($this->RowGroup != null)
            if ($this->RowGroup instanceof RowGroup)
                $s .= (' group by ' . $this->RowGroup->toSQLString($dialect,false));

        if ($this->GroupCondition != null)
            if ($this->GroupCondition instanceof QueryConditions)
                $s .= 'having ' . $this->GroupCondition->toSQLString($dialect,false);

        if ($this->Order != null)
            if ($this->Order instanceof RowOrder)
                $s .= (' order by ' . $this->Order->toSQLString($dialect,false));

        if ($this->Limit != null)
            $s = $this->Limit->CreateSQL($s,$dialect);
        return $s;
    }
}
?>