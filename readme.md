# Standalone Selifa Database Interface #

### Introduction ###
Database connector and utilities classes designed for Selifa Framework but adapted for standalone usage.

### Usage Instructions ###

Install using composer:

```
composer require selifa/data-sa
```

Only depends on native database functions provided by php. Install it using your OS's package installer.

Simple usage example:

```php
<?php
include('vendor/autoload.php');

use RBS\Selifa\Data\DBI;

SelifaDBI::Initialize(array(
      "DefaultDatabase" => "Default",
      "DisableError" => false,
      "Connections" => array(
          "Default" => array(
              "type" => "mysql",
              "host" => "dbhost",
              "name" => "dbname",
              "user" => "username",
              "pass" => "userpassword"
          )
      )
));

$db = DBI::Get();
$data = $db->Prepare("SELECT * FROM Employees")->GetData();
while ($row = $data->Fetch())
{
    echo $row->EmployeeName."\n";
}

?>
```

As of now, there are two DBMS supported, ```mysql``` and ```pgsql```.

Multiple database is supported. Just add another item in ```Connections``` array above, give it a name beside ```Default```.
To use it inside your code, use

```php
$anotherDb = DBI::Get('YourDBName');
```


### Credits ###
Rinardi B. Sarean, rinardi_1518_sarean@hotmail.com.

## License ###
The MIT License (MIT)

Copyright (c) 2019. Rinardi B. Sarean

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.